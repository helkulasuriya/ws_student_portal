package com.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import services.util.Web_portal_serviceStub.PortalUserDAO;

import com.facade.impl.ILoginFacade;
import com.facade.impl.LoginFacade;
import com.service.util.Utils;

public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private ILoginFacade loginFacade = null;
	private PortalUserDAO loggedInUser = null;

	public LoginServlet() {
		super();
		loginFacade = new LoginFacade();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		super.doGet(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// Access the user inserted credentials.
		String userId = req.getParameter("username");
		String password = req.getParameter("password");

		HttpSession session = req.getSession(true); // Obtain session.

		// Validate user.
		boolean isValid = loginFacade.validateUser(userId, password);
		// Forward user to appropriate page.
		this.validateAndForwardUser(isValid, session, req, resp);
	}

	private void validateAndForwardUser(boolean isValid, HttpSession session,
			HttpServletRequest req, HttpServletResponse resp) {
		
		try {
			if (isValid) {
				// If login is valid set logged in user object to session.
				loggedInUser = loginFacade.getRegisteredUser();
				session.setAttribute("loggedInUser", loggedInUser);				

				// Redirect the user to home page.
				resp.sendRedirect("./index.jsp");
				
			} else {
				this.setLoginAttempts(session);
				// Redirect the user back to login page.
				resp.sendRedirect("./login.jsp");
			}
		} catch (Exception e) {
//			Utils.intLog4J(getClass()).error("Error in fowarding request.", e);
			e.printStackTrace();
		}
	}

	private Integer setLoginAttempts(HttpSession session) {
		// Retrieve attempted login value.
		Integer loginAttempts = (Integer) session.getAttribute("loginAttempts");

		// Set login attempts to session.
		if (session.getAttribute("loginAttempts") != null) {
			loginAttempts += 1;
			session.setAttribute("loginAttempts", loginAttempts);
		} else {
			loginAttempts = new Integer(1);
			session.setAttribute("loginAttempts", loginAttempts);
		}
		return loginAttempts;
	}

}

package com.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.service.util.Utils;


/**
 * Servlet implementation class MainServlet
 */
public final class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	   static Logger logger=null;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainServlet() {
        super();
        logger =Utils.getInstance().intLog4J(MainServlet.class);

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    
	    
	    logger.info("Hit the GET method.");
	    String nextJSP = "/test.jsp";
	    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
	    dispatcher.forward(request,response);
	    logger.warn("Request forwared.");
	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

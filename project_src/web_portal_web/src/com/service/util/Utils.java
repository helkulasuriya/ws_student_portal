package com.service.util;





/*
 * Student ID : s3397469 CourseDAO Code : COSC2278/2279 Assignment : 1 Description: A Utils class to share common
 * functionalities
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/*
 * Implemented as a singleton
 */
public class Utils
{

    private static Utils utils = null;
    private Logger utilLogger = null;
    Properties propertes = new Properties();
    private static String DECIMAL_FORMAT = "##.####";

    private Utils()
    {
        utilLogger = intLog4J(this.getClass());
//        initProperties();
    }
    
    public static Utils getInstance()
    {
        if (utils == null)
            utils = new Utils();

        return utils;
    }    

    /**
     *  Use to provide the Logger based on each class
     * @param classForLog Class : Used to specify the class 
     * @return Logger : a log4j logger 
     */
    public Logger intLog4J(Class classForLog)
    {
        
        final Logger logger = Logger.getLogger("Web Portal : "+classForLog.getName());
        PropertyConfigurator.configure("log4j.properties");
        return logger;
    }

    /**
     *  Used to initialize common properties
     */    
    private void initProperties()
    {
        utilLogger.info("Reading the default properties file: " + "build.properties");

        File propertiesFile = new File("build.properties");

        InputStream in = null;
        try
        {
            in = new FileInputStream(propertiesFile);
            propertes.load(in);
            in.close();
            utilLogger.info("Default properties loaded successfully.");
        }
        catch (FileNotFoundException e)
        {
            utilLogger.error("Error while loading the default properties file: build.properties, File Not Found",e);
        }
        catch (IOException e)
        {
            utilLogger.error("Error while loading the default properties file: build.properties",e);

        }
    }

    /**
     *  Use to obtain the values from build.properties 
     * @param key String : property key to obtain the value
     * @return String : value
     */
    public String getProperty(String key)
    {
        return propertes.getProperty(key);
    }

    /**
     * Use to format the amount to decimal points
     * @param value String : property key to obtain the value
     * @return String : formatted value
     */
    public String formatAmout(String value)
    {
        try
        {

            DecimalFormat currencyFormat = new DecimalFormat(DECIMAL_FORMAT);
            return currencyFormat.format(Double.valueOf(value));
        }
        catch (NumberFormatException e)
        {
            utilLogger.error("Error while formating number.", e);
        }
        return "0.0";
    }


    /**
     * Use to obtain the symantec key for encryption
     * @return byte[] : key after converting to a byte array
     */
    public byte[] getEncryptionKey()
    {
        return getProperty("encryption.key").getBytes();
    }
}

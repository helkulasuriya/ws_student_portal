package com.facade.impl;

import org.apache.log4j.Logger;

import services.util.Web_portal_serviceStub;
import services.util.Web_portal_serviceStub.PortalUserDAO;

import com.service.util.Utils;

public class LoginFacade implements ILoginFacade {

	//private Logger utilLogger = Utils.intLog4J(getClass());

	private PortalUserDAO registeredUser = null;
	
	public static void main(String args[]){
		LoginFacade lf = new LoginFacade();
		boolean a = lf.getUserObject("s335432", "world1");
		System.out.println(a);
	}

	@Override
	public boolean validateUser(String userName, String password) {

		boolean isValid = this.getUserObject(userName, password);
		return isValid;
	}
	
	@Override
	public PortalUserDAO getRegisteredUser() {
		return registeredUser;
	}

	private boolean getUserObject(String userName, String password) {
		boolean isValid = false;

		try {
			Web_portal_serviceStub stub = new Web_portal_serviceStub(
					"http://localhost:8787/axis2/services/web_portal_service");
			registeredUser = stub.validateUser(userName, password);
			
			if (registeredUser != null)
				isValid = true;

		} catch (Exception e) {
			//utilLogger.error("Error in accessing the web service", e);
			e.printStackTrace();
		}

		return isValid;
	}

}

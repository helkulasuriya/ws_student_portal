package com.facade.impl;

import services.util.Web_portal_serviceStub.PortalUserDAO;

public interface ILoginFacade {

	public boolean validateUser(String userName, String password);

	public PortalUserDAO getRegisteredUser();
}

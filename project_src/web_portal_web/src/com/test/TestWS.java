package com.test;

import java.rmi.RemoteException;


import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;

import services.util.Web_portal_serviceStub;
import services.util.Web_portal_serviceStub.PortalUserDAO;

import com.service.util.Utils;

public class TestWS
{

    public TestWS()
    {
        logger=Utils.intLog4J(this.getClass()); 
        


        
    }

    private Logger logger=null;
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        TestWS testWS=new TestWS();
        
        testWS.userLogin();
    }

    void userLogin(){
        
    Web_portal_serviceStub stub = null;
    
    try
    {
                    
        stub = new Web_portal_serviceStub(Utils.getInstance().getProperty("wsdl.login.uri"));
        PortalUserDAO user=null;
        for(int x=0;x<10;x++){
         user=stub.validateUser(x+" useName", "password", "s");
        }
        
        
        
        
        logger.info("Calling webservice : "+user.getName());

    }
    catch (AxisFault e)
    {
        logger.error("Axis Fault while calling the ADB webservice : "+e.getMessage());
        e.printStackTrace();
    }
    catch (RemoteException e)
    {
        logger.error("Remote Exception while calling the ADB webservice : "+e.getMessage());
        e.printStackTrace();
    }
    catch (Exception e)
    {
        logger.error("Error while calling the ADB webservice : "+e.getMessage());
        e.printStackTrace();
    }


}

}

<%@page import="services.util.Web_portal_serviceStub.PortalUserDAO"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
	<link media="all" type="text/css" href="css/core.css" rel="stylesheet"></link>

	<script src="js/jquery-1.8.3.min.js" type="text/javascript"></script>	
	<script type="text/javascript">
		$(document).ready(function (){
			
		  $('.menu_link').click(function(e){
		  var requestMapping=$(this).attr('href');
		     alert('Request will be sent to the servlet with the mapping id of :::::: '+requestMapping);
		     
		     $('#body_frame').attr('src', 'http://localhost:8787/web_portal_web/MainServlet?request='+requestMapping);
		     e.preventDefault();
		   });		
		});
		
	</script>
</head>
<body >
<% 
	PortalUserDAO loggedInUser = (PortalUserDAO) session.getAttribute("loggedInUser");
%>
<div class="menue_div_top">
<table >
  <tbody>
	   <tr>
      <td><img id="avc_logo_image" src="img/title_main.png" style=" padding-bottom: 10px; border:0px"></td>
      
      <td>
	  
	  <div class="menue_div">
  <ul class="Superfish menu">
    <li class="selected current tab"><a class="menu_link"  href="/home" >Home</a></li>
    <li class="selected current tab"><a class="menu_link"  href="/register" >Register for a courses</a></li>
    <li class="tab"><a class="menu_link"  href="/searchMain">Search / View</a>
      <ul class="children">
        <li><a class="menu_link"  href="/searchAnnouncements" >Search Announcements</a></li>
        <li><a class="menu_link"  href="/searchAssignments" >Search Assignments</a></li>
        <li><a class="menu_link"  href="/searchSubjects" >Search Subjects</a></li>
      </ul>
    </li>
    <li class="tab"><a class="menu_link"  href="/createMain">Create content</a>
      <ul class="children">
        <li><a class="menu_link"  href="/createAnnouncemnt" >Create Announcement</a></li>
        <li><a class="menu_link"  href="/createAssignment" >Create Assignment</a></li>
        <li><a class="menu_link"  href="/createSubject" >Create Subject</a></li>
      </ul>
    </li>
    <li class="tab"><a class="menu_link"  href="/AdministrationMain">Administration</a>
      <ul class="children">
        <li><a class="menu_link"  href="/changePassword" >Change password</a></li>
        <li><a class="menu_link"  href="/loginHistory" >View login history</a></li>
      </ul>
    </li>

  </ul>
</div>
    </tr>
  </tbody>
</table>
</div>
<hr/>
<div id="welcomeMsg">	
	<c:if test="${loggedInUser != null}">
		<c:choose>
			<c:when test="${loggedInUser.userType == 's'}">
				<img id="avc_logo_image" src="img/student.JPG" style=" ">
			</c:when>
			<c:otherwise>
				<img id="avc_logo_image" src="img/prof.JPG" style=" ">
			</c:otherwise>
		</c:choose>
	</c:if>
	Welcome <c:out value="${loggedInUser.name}" />
</div>
<br/>
<div class="menue_div_bottom">
<iframe src="content.html" width="100%" height="800%" id="body_frame" marginheight="0" frameborder="0"></iframe>
</div>
</body>
</html>
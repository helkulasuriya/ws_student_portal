package com.dao;

import java.io.Serializable;

/**
 * DAO for generic user information
 */
public class PortalUserDAO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String userLoginName;
	private String password;
	private String name;
	private String phoneNo;
	private String address;
	private String userType;

	public PortalUserDAO(String userId, String password) {
		this.userLoginName = userId;
		this.password = password;
	}

	public PortalUserDAO(String userId, String password, String name,
			String phoneNo, String address) {
		setUserLoginName(userId);
		this.password = password;
		this.address = address;
		this.name = name;
		this.phoneNo = phoneNo;
	}

	public String getUserLoginName() {
		return userLoginName;
	}

	public void setUserLoginName(String userLoginName) {
		this.userLoginName = userLoginName;

		if ((this.userLoginName != null)
				&& (this.userLoginName.trim().toLowerCase().startsWith("s")))
			this.setUserType("s");
		else if ((this.userLoginName != null)
				&& (this.userLoginName.trim().toLowerCase().startsWith("t")))
			this.setUserType("t");
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}

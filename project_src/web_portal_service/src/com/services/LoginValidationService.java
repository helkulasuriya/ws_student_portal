package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.dao.PortalUserDAO;

/**
 * WS implementation for user account validation
 */
public class LoginValidationService {

	/**
	 * Validate the user against the database data, return the user information
	 * related to the provided user name and password.
	 * 
	 * @param userName
	 *            Login id of the user.
	 * @param password
	 *            Password of the user.
	 * @return PortalUserDAO object with data populated or NULL if user does not
	 *         exist.
	 */
	public PortalUserDAO validateUser(String userName, String password) {

		PortalUserDAO registeredUser = null;

		// Obtain JDBC connection
		Connection con = getConnection();
		// Create sql query
		String sqlQuery = "SELECT u.login_id, u.password, ru.name, ru.user_type, ru.user_contact_no, ru.user_address FROM user_login u INNER JOIN  registered_user ru ON u.registered_user_id=ru.registered_user_id WHERE login_id='"
				+ userName + "' AND password='" + password + "'";

		if (con != null) {
			try {
				PreparedStatement stmt = con.prepareStatement(sqlQuery);
				ResultSet rs = stmt.executeQuery();

				if (rs == null)
					return null;

				while (rs.next()) {
					registeredUser = new PortalUserDAO(
							rs.getString("login_id"), rs.getString("password"),
							rs.getString("name"),
							rs.getString("user_contact_no"),
							rs.getString("user_address"));
				}

			} catch (Exception e) {
				Logger.getLogger(LoginValidationService.class.getName()).log(
						Level.SEVERE, null, e);
			}

			return registeredUser;

		} else {
			return null;
		}
	}

	/**
	 * Obtain the JDBC connection from the connection pool.
	 * 
	 * @return JDBC connection object.
	 */
	private Connection getConnection() {

		Context initContext = null;
		Connection con = null;

		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");

			DataSource ds = (DataSource) envContext
					.lookup("jdbc/student_portal");

			con = ds.getConnection();

		} catch (Exception e) {
			Logger.getLogger(LoginValidationService.class.getName()).log(
					Level.SEVERE, null, e);
		}

		return con;
	}
	
	public String helloWorld(){
		return "Hello World..!!";
	}

	// private PortalUserDAO authenticate() {
	// PortalUserDAO portalUser = null;
	//
	// Context initContext;
	// try {
	// initContext = new InitialContext();
	// Context envContext = (Context) initContext.lookup("java:/comp/env");
	// DataSource ds = (DataSource) envContext.lookup("jdbc/TestDB");
	// Connection conn = ds.getConnection();
	// System.out.println("xxxxxx" + conn == null);
	//
	// String selectSQL = "select id,foo,bar from testdata;";
	//
	// PreparedStatement preparedStatement = conn
	// .prepareStatement(selectSQL);
	// // preparedStatement.setInt(1, 1001);
	//
	// // execute select SQL stetement
	// ResultSet rs = preparedStatement.executeQuery();
	//
	// while (rs.next()) {
	//
	// String userid = rs.getString("id");
	// String username = rs.getString("foo");
	// String bar = rs.getString("bar");
	//
	// System.out.println("id : " + userid);
	// System.out.println("foo : " + username);
	// System.out.println("bar : " + bar);
	//
	// }
	//
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// return portalUser;
	// }
}

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `student_portal` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `student_portal` ;

-- -----------------------------------------------------
-- Table `student_portal`.`registered_user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`registered_user` (
  `registered_user_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `user_type` VARCHAR(45) NOT NULL ,
  `user_contact_no` VARCHAR(45) NOT NULL ,
  `user_address` VARCHAR(45) NOT NULL ,
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`registered_user_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_portal`.`user_login`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`user_login` (
  `login_id` VARCHAR(25) NOT NULL ,
  `password` VARCHAR(45) NOT NULL ,
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `registered_user_id` INT NOT NULL ,
  PRIMARY KEY (`login_id`) ,
  INDEX `login_fk_1` (`registered_user_id` ASC) ,
  CONSTRAINT `login_fk_1`
    FOREIGN KEY (`registered_user_id` )
    REFERENCES `student_portal`.`registered_user` (`registered_user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_portal`.`course`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course` (
  `course_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `location` VARCHAR(20) NOT NULL ,
  `name` VARCHAR(20) NOT NULL ,
  `credit` VARCHAR(10) NOT NULL ,
  `time` VARCHAR(20) NOT NULL ,
  `term` VARCHAR(20) NOT NULL ,
  `created_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`course_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_portal`.`course_attributes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_attributes` (
  `attribute_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `course_id` INT(11) NOT NULL ,
  `attribute_type` INT(11) NOT NULL COMMENT 'Syllabus, note, announcement, other' ,
  `description` VARCHAR(50) NOT NULL ,
  `value` BLOB NOT NULL ,
  `created_by` INT(11) NOT NULL ,
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`attribute_id`) ,
  INDEX `course_id_fk` (`course_id` ASC) ,
  CONSTRAINT `course_id_fk`
    FOREIGN KEY (`course_id` )
    REFERENCES `student_portal`.`course` (`course_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_portal`.`course_enrollment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_enrollment` (
  `enrollment_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `registered_user_id` INT(11) NOT NULL ,
  `course_id` INT(11) NOT NULL ,
  `final_grade` VARCHAR(10) NOT NULL ,
  `enrolled_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`enrollment_id`) ,
  INDEX `student_id_fk` (`registered_user_id` ASC) ,
  INDEX `course_id_fk2` (`course_id` ASC) ,
  CONSTRAINT `student_id_fk`
    FOREIGN KEY (`registered_user_id` )
    REFERENCES `student_portal`.`registered_user` (`registered_user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `course_id_fk2`
    FOREIGN KEY (`course_id` )
    REFERENCES `student_portal`.`course` (`course_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_portal`.`course_practice`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_practice` (
  `practice_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `course_id` INT(11) NOT NULL ,
  `name` VARCHAR(20) NOT NULL ,
  `practice_type` VARCHAR(10) NOT NULL ,
  `date` VARCHAR(20) NOT NULL ,
  `description` VARCHAR(100) NOT NULL ,
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`practice_id`) ,
  INDEX `course_id_fk3` (`course_id` ASC) ,
  CONSTRAINT `course_id_fk3`
    FOREIGN KEY (`course_id` )
    REFERENCES `student_portal`.`course` (`course_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_portal`.`course_participation`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_participation` (
  `participation_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `registered_user_id` INT(11) NOT NULL ,
  `practice_id` INT(11) NOT NULL ,
  `grade` VARCHAR(10) NOT NULL ,
  `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`participation_id`) ,
  INDEX `registered_user_fk2` (`registered_user_id` ASC) ,
  INDEX `fk_course_practice1` (`practice_id` ASC) ,
  CONSTRAINT `registered_user_fk2`
    FOREIGN KEY (`registered_user_id` )
    REFERENCES `student_portal`.`registered_user` (`registered_user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_practice1`
    FOREIGN KEY (`practice_id` )
    REFERENCES `student_portal`.`course_practice` (`practice_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_portal`.`course_prerequest`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_prerequest` (
  `prereq_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `course_id` INT(11) NOT NULL ,
  `description` VARCHAR(200) NOT NULL ,
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`prereq_id`) ,
  INDEX `fk_course4` (`course_id` ASC) ,
  CONSTRAINT `fk_course4`
    FOREIGN KEY (`course_id` )
    REFERENCES `student_portal`.`course` (`course_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_portal`.`course_teaching`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_teaching` (
  `teaching_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `course_id` INT(11) NOT NULL ,
  `registered_user_id` INT(11) NOT NULL ,
  `assigned_on` VARCHAR(45) NOT NULL ,
  `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`teaching_id`) ,
  INDEX `fk_course_teaching_course1` (`course_id` ASC) ,
  INDEX `fk_course_teaching_registered_user1` (`registered_user_id` ASC) ,
  CONSTRAINT `fk_course_teaching_course1`
    FOREIGN KEY (`course_id` )
    REFERENCES `student_portal`.`course` (`course_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_teaching_registered_user1`
    FOREIGN KEY (`registered_user_id` )
    REFERENCES `student_portal`.`registered_user` (`registered_user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

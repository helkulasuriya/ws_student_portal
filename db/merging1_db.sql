-- MySQL dump 10.13  Distrib 5.5.9, for Win32 (x86)
--
-- Host: localhost    Database: student_portal
-- ------------------------------------------------------
-- Server version	5.5.8

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `student_portal`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `student_portal` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `student_portal`;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `course_id` int(10) NOT NULL AUTO_INCREMENT,
  `location` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `credit` varchar(10) NOT NULL,
  `time` varchar(20) NOT NULL,
  `term` varchar(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'City Campus','DB Designing','50','15:30 THUS','Mid','2013-05-25 13:00:59'),(2,'City Campus','Sysrem Designing','50','17:30 MON','Mid','2013-05-25 13:01:43'),(3,'City Campus','SEC','50','17:30 WED','Mid','2013-05-27 02:59:18'),(4,'City Campus','Web Services','50','17:30 THU','First','2013-05-27 23:26:07');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_attributes`
--

DROP TABLE IF EXISTS `course_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_attributes` (
  `attribute_id` int(10) NOT NULL AUTO_INCREMENT,
  `course_id` int(10) NOT NULL,
  `attribute_type` varchar(20) DEFAULT NULL,
  `description` varchar(50) NOT NULL,
  `value` blob NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`attribute_id`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `course_attributes_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_attributes`
--

LOCK TABLES `course_attributes` WRITE;
/*!40000 ALTER TABLE `course_attributes` DISABLE KEYS */;
INSERT INTO `course_attributes` VALUES (1,1,'DESCRIPTION','Contains course notes','<div align=\"center\"><u><b>What is Neuroscience </b></u><br></div>Neuroscience is the scientific study of the nervous system.[1] Traditionally, neuroscience has been seen as a branch of biology. However, it is currently an interdisciplinary science that collaborates with other fields such as chemistry, computer science, engineering, linguistics, mathematics, medicine and allied disciplines, philosophy, physics, and psychology. The term neurobiology is usually used interchangeably with the term neuroscience, although the former refers specifically to the biology of the nervous system, whereas the latter refers to the entire science of the nervous system.<br><br><a href=\"https://en.wikipedia.org/wiki/Neuroscience\">https://en.wikipedia.org/wiki/Neuroscience</a>',1,'2013-05-25 13:14:56','25-Mar-2013 13:55'),(2,1,'NOTE','Contains course notes','<p><strong>Neuroscience for Kids</strong> has been created for all students and teachers who would like to learn about the nervous system.</p><p><img src=\"http://faculty.washington.edu/chudler/gif/mainh.gif\" class=\"flr\" alt=\"\"> Discover the exciting world of the brain, spinal cord, neurons and the senses. Use the experiments, activities and games to help you learn about the nervous system. There are plenty of links to other web sites for you to explore.</p><br><a href=\"http://faculty.washington.edu/chudler/neurok.html\">http://faculty.washington.edu/chudler/neurok.html</a>',1,'2013-05-25 13:16:37','27-Mar-2013 13:55');
/*!40000 ALTER TABLE `course_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_enrollment`
--

DROP TABLE IF EXISTS `course_enrollment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_enrollment` (
  `enrollment_id` int(10) NOT NULL AUTO_INCREMENT,
  `registered_user_id` int(10) NOT NULL,
  `course_id` int(10) NOT NULL,
  `final_grade` varchar(10) DEFAULT NULL,
  `enrolled_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`enrollment_id`),
  KEY `course_id` (`course_id`),
  KEY `registered_user_id` (`registered_user_id`),
  CONSTRAINT `course_enrollment_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`),
  CONSTRAINT `course_enrollment_ibfk_2` FOREIGN KEY (`registered_user_id`) REFERENCES `registered_user` (`registered_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_enrollment`
--

LOCK TABLES `course_enrollment` WRITE;
/*!40000 ALTER TABLE `course_enrollment` DISABLE KEYS */;
INSERT INTO `course_enrollment` VALUES (1,3,1,'78','2013-05-28 16:16:49'),(2,3,2,'Failed','2013-05-28 16:16:49'),(3,3,3,'80','2013-05-30 05:48:43');
/*!40000 ALTER TABLE `course_enrollment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_participation`
--

DROP TABLE IF EXISTS `course_participation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_participation` (
  `participation_id` int(10) NOT NULL AUTO_INCREMENT,
  `registered_user_id` int(10) NOT NULL,
  `practice_id` int(10) NOT NULL,
  `grade` varchar(10) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`participation_id`),
  KEY `practice_id` (`practice_id`),
  KEY `registered_user_id` (`registered_user_id`),
  CONSTRAINT `course_participation_ibfk_1` FOREIGN KEY (`practice_id`) REFERENCES `course_practice` (`practice_id`),
  CONSTRAINT `course_participation_ibfk_2` FOREIGN KEY (`registered_user_id`) REFERENCES `registered_user` (`registered_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_participation`
--

LOCK TABLES `course_participation` WRITE;
/*!40000 ALTER TABLE `course_participation` DISABLE KEYS */;
INSERT INTO `course_participation` VALUES (1,3,1,'80','2013-05-28 23:59:08'),(2,3,2,'73','2013-05-29 14:36:34');
/*!40000 ALTER TABLE `course_participation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_practice`
--

DROP TABLE IF EXISTS `course_practice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_practice` (
  `practice_id` int(10) NOT NULL AUTO_INCREMENT,
  `course_id` int(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `practice_type` varchar(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`practice_id`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `course_practice_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_practice`
--

LOCK TABLES `course_practice` WRITE;
/*!40000 ALTER TABLE `course_practice` DISABLE KEYS */;
INSERT INTO `course_practice` VALUES (1,3,'Assignment 1','ASSIGNMENT','JUN 7, 2013','Web service assignment  1','2013-05-29 00:06:02'),(2,3,'Quiz 3','QUIZ','MAY 21, 2013','Web service quiz 3','2013-05-29 00:06:02'),(3,4,'Final Exam','FINAL','APR 4, 2013','Web service final exam','2013-05-28 18:26:05');
/*!40000 ALTER TABLE `course_practice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_prerequest`
--

DROP TABLE IF EXISTS `course_prerequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_prerequest` (
  `prereq_id` int(10) NOT NULL AUTO_INCREMENT,
  `prerequest_course_id` int(10) NOT NULL,
  `course_id` int(10) NOT NULL,
  `description` varchar(200) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`prereq_id`),
  KEY `course_id` (`course_id`),
  KEY `course_prerequest_fk2` (`prerequest_course_id`),
  CONSTRAINT `course_prerequest_fk2` FOREIGN KEY (`prerequest_course_id`) REFERENCES `course` (`course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `course_prerequest_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_prerequest`
--

LOCK TABLES `course_prerequest` WRITE;
/*!40000 ALTER TABLE `course_prerequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_prerequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_teaching`
--

DROP TABLE IF EXISTS `course_teaching`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_teaching` (
  `teaching_id` int(10) NOT NULL AUTO_INCREMENT,
  `course_id` int(10) NOT NULL,
  `registered_user_id` int(10) NOT NULL,
  `assigned_on` varchar(45) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`teaching_id`),
  KEY `course_id` (`course_id`),
  KEY `registered_user_id` (`registered_user_id`),
  CONSTRAINT `course_teaching_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`),
  CONSTRAINT `course_teaching_ibfk_2` FOREIGN KEY (`registered_user_id`) REFERENCES `registered_user` (`registered_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_teaching`
--

LOCK TABLES `course_teaching` WRITE;
/*!40000 ALTER TABLE `course_teaching` DISABLE KEYS */;
INSERT INTO `course_teaching` VALUES (1,1,1,'23 Mon 2013','2013-05-25 13:05:31'),(2,2,1,'23 Mon 2013','2013-05-25 13:05:37');
/*!40000 ALTER TABLE `course_teaching` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registered_user`
--

DROP TABLE IF EXISTS `registered_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registered_user` (
  `registered_user_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `user_type` varchar(45) NOT NULL,
  `user_contact_no` varchar(45) NOT NULL,
  `user_address` varchar(45) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`registered_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registered_user`
--

LOCK TABLES `registered_user` WRITE;
/*!40000 ALTER TABLE `registered_user` DISABLE KEYS */;
INSERT INTO `registered_user` VALUES (1,'John Peter','Teacher','0934345644','CBD Melbounre','2013-05-25 13:03:33'),(2,'Peter Greg','Teacher','23564534','CBD Melbounre','2013-05-25 13:03:46'),(3,'hel','Student','01223434','Carlton','2013-05-26 18:40:30');
/*!40000 ALTER TABLE `registered_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_login`
--

DROP TABLE IF EXISTS `user_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_login` (
  `login_id` varchar(25) NOT NULL,
  `password` varchar(45) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `registered_user_id` int(10) NOT NULL,
  `last_login_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`login_id`),
  KEY `registered_user_id` (`registered_user_id`),
  CONSTRAINT `user_login_ibfk_1` FOREIGN KEY (`registered_user_id`) REFERENCES `registered_user` (`registered_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login`
--

LOCK TABLES `user_login` WRITE;
/*!40000 ALTER TABLE `user_login` DISABLE KEYS */;
INSERT INTO `user_login` VALUES ('s335432','world1','2013-05-30 07:11:15',3,'2013-05-30 07:11:15'),('t2345','world2','2013-05-28 23:13:28',2,'2013-05-27 19:30:00'),('t4451','hello1','2013-05-26 18:43:34',1,NULL);
/*!40000 ALTER TABLE `user_login` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-01 12:33:18

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `student_portal` DEFAULT CHARACTER SET latin1 ;
USE `student_portal` ;

-- -----------------------------------------------------
-- Table `student_portal`.`course`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course` (
  `course_id` INT(10) NOT NULL AUTO_INCREMENT ,
  `location` VARCHAR(20) NOT NULL ,
  `name` VARCHAR(20) NOT NULL ,
  `credit` VARCHAR(10) NOT NULL ,
  `time` VARCHAR(20) NOT NULL ,
  `term` VARCHAR(20) NOT NULL ,
  `created_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`course_id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `student_portal`.`course_attributes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_attributes` (
  `attribute_id` INT(10) NOT NULL AUTO_INCREMENT ,
  `course_id` INT(10) NOT NULL ,
  `attribute_type` VARCHAR(20) NULL DEFAULT NULL ,
  `description` VARCHAR(50) NOT NULL ,
  `value` BLOB NOT NULL ,
  `created_by` INT(10) NOT NULL ,
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `last_modified` VARCHAR(25) NULL DEFAULT NULL ,
  PRIMARY KEY (`attribute_id`) ,
  INDEX `course_id` (`course_id` ASC) ,
  CONSTRAINT `course_attributes_ibfk_1`
    FOREIGN KEY (`course_id` )
    REFERENCES `student_portal`.`course` (`course_id` ))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `student_portal`.`registered_user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`registered_user` (
  `registered_user_id` INT(10) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `user_type` VARCHAR(45) NOT NULL ,
  `user_contact_no` VARCHAR(45) NOT NULL ,
  `user_address` VARCHAR(45) NOT NULL ,
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`registered_user_id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `student_portal`.`course_enrollment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_enrollment` (
  `enrollment_id` INT(10) NOT NULL AUTO_INCREMENT ,
  `registered_user_id` INT(10) NOT NULL ,
  `course_id` INT(10) NOT NULL ,
  `final_grade` VARCHAR(10) NULL DEFAULT NULL ,
  `enrolled_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`enrollment_id`) ,
  INDEX `course_id` (`course_id` ASC) ,
  INDEX `registered_user_id` (`registered_user_id` ASC) ,
  CONSTRAINT `course_enrollment_ibfk_1`
    FOREIGN KEY (`course_id` )
    REFERENCES `student_portal`.`course` (`course_id` ),
  CONSTRAINT `course_enrollment_ibfk_2`
    FOREIGN KEY (`registered_user_id` )
    REFERENCES `student_portal`.`registered_user` (`registered_user_id` ))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `student_portal`.`course_practice`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_practice` (
  `practice_id` INT(10) NOT NULL AUTO_INCREMENT ,
  `course_id` INT(10) NOT NULL ,
  `name` VARCHAR(20) NOT NULL ,
  `practice_type` VARCHAR(10) NOT NULL ,
  `date` VARCHAR(20) NOT NULL ,
  `description` VARCHAR(100) NOT NULL ,
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`practice_id`) ,
  INDEX `course_id` (`course_id` ASC) ,
  CONSTRAINT `course_practice_ibfk_1`
    FOREIGN KEY (`course_id` )
    REFERENCES `student_portal`.`course` (`course_id` ))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `student_portal`.`course_participation`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_participation` (
  `participation_id` INT(10) NOT NULL AUTO_INCREMENT ,
  `registered_user_id` INT(10) NOT NULL ,
  `practice_id` INT(10) NOT NULL ,
  `grade` VARCHAR(10) NULL DEFAULT NULL ,
  `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`participation_id`) ,
  INDEX `practice_id` (`practice_id` ASC) ,
  INDEX `registered_user_id` (`registered_user_id` ASC) ,
  CONSTRAINT `course_participation_ibfk_1`
    FOREIGN KEY (`practice_id` )
    REFERENCES `student_portal`.`course_practice` (`practice_id` ),
  CONSTRAINT `course_participation_ibfk_2`
    FOREIGN KEY (`registered_user_id` )
    REFERENCES `student_portal`.`registered_user` (`registered_user_id` ))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `student_portal`.`course_prerequest`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_prerequest` (
  `prereq_id` INT(10) NOT NULL AUTO_INCREMENT ,
  `prerequest_course_id` INT(10) NOT NULL ,
  `course_id` INT(10) NOT NULL ,
  `description` VARCHAR(200) NOT NULL ,
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`prereq_id`) ,
  INDEX `course_id` (`course_id` ASC) ,
  INDEX `course_prerequest_fk2` (`prerequest_course_id` ASC) ,
  CONSTRAINT `course_prerequest_fk2`
    FOREIGN KEY (`prerequest_course_id` )
    REFERENCES `student_portal`.`course` (`course_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `course_prerequest_ibfk_1`
    FOREIGN KEY (`course_id` )
    REFERENCES `student_portal`.`course` (`course_id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `student_portal`.`course_teaching`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`course_teaching` (
  `teaching_id` INT(10) NOT NULL AUTO_INCREMENT ,
  `course_id` INT(10) NOT NULL ,
  `registered_user_id` INT(10) NOT NULL ,
  `assigned_on` VARCHAR(45) NOT NULL ,
  `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`teaching_id`) ,
  INDEX `course_id` (`course_id` ASC) ,
  INDEX `registered_user_id` (`registered_user_id` ASC) ,
  CONSTRAINT `course_teaching_ibfk_1`
    FOREIGN KEY (`course_id` )
    REFERENCES `student_portal`.`course` (`course_id` ),
  CONSTRAINT `course_teaching_ibfk_2`
    FOREIGN KEY (`registered_user_id` )
    REFERENCES `student_portal`.`registered_user` (`registered_user_id` ))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `student_portal`.`user_login`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `student_portal`.`user_login` (
  `login_id` VARCHAR(25) NOT NULL ,
  `password` VARCHAR(45) NOT NULL ,
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `registered_user_id` INT(10) NOT NULL ,
  `last_login_time` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`login_id`) ,
  INDEX `registered_user_id` (`registered_user_id` ASC) ,
  CONSTRAINT `user_login_ibfk_1`
    FOREIGN KEY (`registered_user_id` )
    REFERENCES `student_portal`.`registered_user` (`registered_user_id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

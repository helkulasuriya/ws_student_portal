package com.dao;

import java.io.Serializable;

/**
 * Class to handle pre-requisite course information.
 */
public class PrerequestCourseDAO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int prereqId;
	private int prereqCourseId;
	private int courseId;
	private String description;

	public int getPrereqId() {
		return prereqId;
	}

	public void setPrereqId(int prereqId) {
		this.prereqId = prereqId;
	}

	public int getPrereqCourseId() {
		return prereqCourseId;
	}

	public void setPrereqCourseId(int prereqCourseId) {
		this.prereqCourseId = prereqCourseId;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}

package com.util;





import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



/*
 * Implemented as a singleton
 */
public class Utils
{

    private static Utils utils = null;

    private static String DATE_STAMP_FORMAT = "yyyy/MM/dd HH:mm:ss";

    private Utils()
    {    }
    
    public static Utils getInstance()
    {
        if (utils == null)
            utils = new Utils();

        return utils;
    }    
    
public String getDateStamp(){
	DateFormat dateFormat = new SimpleDateFormat(DATE_STAMP_FORMAT);
	Date now = new Date();
	return(dateFormat.format(now));
}
}

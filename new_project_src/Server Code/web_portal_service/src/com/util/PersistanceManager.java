package com.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class PersistanceManager
{

    private static PersistanceManager persistanceManager = null;
    

    private String dataSourceURL = "jdbc/TestDB";

    public static PersistanceManager getPersistanceManager()
    {
        if (persistanceManager == null)
            persistanceManager = new PersistanceManager();

        return persistanceManager;
    }

    public ResultSet getResultSet(String sqlQuery, Map<Integer,Object> parameterList)  
    {

        Connection localConnection=getConnection();
        ResultSet resultSet=null;
        if (localConnection == null)
            return null;

        PreparedStatement preparedStatement;
        
        try
        {
            preparedStatement = this.getConnection().prepareStatement(sqlQuery);
        
            System.out.println("SQL : "+ sqlQuery );

        setParams(parameterList,preparedStatement);

        // execute select SQL stetement
         resultSet = preparedStatement.executeQuery();
        
        } catch (SQLException e)        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally{
            closeConnection(localConnection);
        }
        
        return resultSet;

    }

    
    
    public int insertRecord(String sqlQuery, Map<Integer,Object> parameterList)  {
        
        Connection localConnection=getConnection();
        PreparedStatement preparedStatement = null;
        
        int recordCount=0;
        
        if (localConnection == null)
            return recordCount;
        
 
        try {

            preparedStatement = localConnection.prepareStatement(sqlQuery);
 
            setParams(parameterList,preparedStatement);
            
            System.out.println("SQL : "+ sqlQuery );

            
            // execute insert SQL stetement
            recordCount=preparedStatement.executeUpdate();
 
            System.out.println("Record is inserted into table!");
 
        } catch (SQLException e) {
                e.printStackTrace();
 
        } finally {
            closeConnection(localConnection); 
        }
        
        return recordCount;
 
    }
 
    
    
    private void closeConnection(Connection connection){
        try
        {
            connection.commit();
            connection.close();
        }
        catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private Connection getConnection()
    {
        Connection connection=null;
        try
        {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            DataSource ds = (DataSource) envContext.lookup(this.dataSourceURL);

            connection=(ds.getConnection());
        }
        catch (NamingException e)
        {
            e.printStackTrace();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        
        return connection;

    }

    private void setParams(Map<Integer,Object> paraMap, PreparedStatement preparedStatement) throws SQLException
    {

        for (Integer index : paraMap.keySet())
        {
            Object value = paraMap.get(index);

                if (value == null)
                    return;
                else if (value instanceof String)
                    preparedStatement.setString(index, value.toString());
                else if (value instanceof Float)
                    preparedStatement.setFloat(index, Float.valueOf(value.toString()));
                // System.out.println(value+" F ");
                else if (value instanceof Boolean)
                    preparedStatement.setBoolean(index, Boolean.valueOf(value.toString()));
                // System.out.println(value+" B ");
                else if (value instanceof Number)
                    preparedStatement.setInt(index, Integer.valueOf(value.toString()));
                // System.out.println(value+" S ");
                else if (value instanceof Byte[])
                    preparedStatement.setBytes(index, (value.toString().getBytes()));
                // System.out.println(value+" B[] ");
            }
        }




}

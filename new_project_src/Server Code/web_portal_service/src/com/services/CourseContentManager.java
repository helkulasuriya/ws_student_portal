package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.dao.CourseDAO;
import com.util.Utils;

/**
 * WS implementation for user account validation
 */
public class CourseContentManager {
	//

	public boolean updateCourse(CourseDAO courseDAO) {

		boolean response = false;

		if (courseDAO != null) {
			System.out.println("Calling update");
			response = (modifyCourse(courseDAO));
		} else {
			System.out.println("Invalid");
		}

		return response;
	}

	public boolean updateCourseData(int courseId, int practiceId, String date,
			String percentage) {

		boolean response = false;

		if (date != null && courseId > 0 && practiceId > 0) {
			response = (updateData(courseId, practiceId, date, percentage));
		} else {
			System.out.println("Invalid");
		}

		return response;
	}

	private boolean updateData(int courseId, int practiceId, String date,
			String percentage) {

		boolean reslut = false;

		Context initContext;
		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext
					.lookup("jdbc/student_portal");
			Connection conn = ds.getConnection();

			String updatrSQL = "update student_portal.course_practice "
					+ "set date= \'" + date + "\' ," + " percentage= \'"
					+ percentage + "\'" + " where 1=1 and " + " practice_id="
					+ courseId;

			if (conn != null) {
				try {
					PreparedStatement stmt = conn.prepareStatement(updatrSQL);
					int affectedRows = stmt.executeUpdate();

					if (affectedRows == 1) {
						reslut = true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					conn.close();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reslut;
	}

	private boolean modifyCourse(CourseDAO courseDao) {

		boolean reslut = false;

		Context initContext;
		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext
					.lookup("jdbc/student_portal");
			Connection conn = ds.getConnection();

			String selectSQL = "UPDATE student_portal.course "
					+ "SET location =? ," + "title =? , " + "credit =?,  "
					+ "time =? , " + "term =? , " + "last_modified =?  "
					+ " WHERE 1=1 AND " + "course_id=? ";

			PreparedStatement preparedStatement = conn
					.prepareStatement(selectSQL);

			System.out.println(selectSQL);
			System.out.println(courseDao.getLastModified());
			System.out.println(courseDao.getCourseId());

			preparedStatement.setString(1, courseDao.getLocation());
			preparedStatement.setString(2, courseDao.getName());
			preparedStatement.setString(3, courseDao.getCredit());
			preparedStatement.setString(4, courseDao.getTime());
			preparedStatement.setString(5, courseDao.getTerm());
			preparedStatement.setString(6, Utils.getInstance().getDateStamp());
			preparedStatement.setInt(7, courseDao.getCourseId());

			// execute select SQL stetement
			int count = preparedStatement.executeUpdate();

			if (count > 0)
				reslut = true;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return reslut;
	}

}

package com.services;

/**
 * GPA calculator service.
 */
public class GPACalculatorService {

	/**
	 * Calculates GP value
	 */
	int getGP(int GPValue, int subjectCreditPoint) {

		return GPValue * subjectCreditPoint;
	}

	/**
	 * Calculates GPA value
	 */
	int getGPAverage(int sumOfCredits, int sumOfGPValue) {

		return sumOfGPValue / sumOfCredits;
	}

	/*
	 * Legend : High distinction - HD (80�100) GP = 4 Distinction - DI (70�79) =
	 * 3 Credit - CR (60�69) = 2 Pass - PA (50�59) = 1
	 */
	int getGPV(int marks) {

		int GPValue = 0;

		if (marks >= 50 && marks <= 59)
			GPValue = 1;
		else if (marks >= 60 && marks <= 69)
			GPValue = 2;
		else if (marks >= 70 && marks <= 79)
			GPValue = 2;
		else if (marks >= 80)
			GPValue = 2;
		else
			GPValue = 0;

		return GPValue;
	}

}

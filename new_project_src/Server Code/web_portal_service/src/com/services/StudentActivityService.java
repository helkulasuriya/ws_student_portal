package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.dao.ClassStatDAO;
import com.dao.EnrollmentDAO;
import com.dao.PracticeDAO;
import com.dao.PrerequestCourseDAO;

/**
 * Service class to handle functionality regarding user enrollment.
 */
public class StudentActivityService {

	public EnrollmentDAO[] getEnrolledCourses(String loginId) {

		List<EnrollmentDAO> enrolledCourses = new ArrayList<EnrollmentDAO>();

		// Obtain JDBC connection
		Connection con = getConnection();
		// Create sql query
		String sqlQuery = "SELECT ce.enrollment_id, ce.registered_user_id, "
				+ "c.course_id, c.name, c.time, ce.final_grade, c.credit FROM student_portal.course_enrollment ce "
				+ "INNER JOIN student_portal.course c ON ce.course_id = c.course_id "
				+ "INNER JOIN student_portal.user_login u ON ce.registered_user_id = u.registered_user_id "
				+ "WHERE u.login_id = '" + loginId + "'";

		if (con != null) {
			try {
				PreparedStatement stmt = con.prepareStatement(sqlQuery);
				ResultSet rs = stmt.executeQuery();

				if (rs == null)
					return null;

				while (rs.next()) {
					EnrollmentDAO course = new EnrollmentDAO();
					course.setEnrollmentId(rs.getInt(1));
					course.setStudentId(rs.getInt(2));
					course.setCourseId(rs.getInt(3));
					course.setCourseName(rs.getString(4));
					course.setCourseTime(rs.getString(5));
					course.setFinalGrade(rs.getString(6));
					course.setCredit(rs.getString(7));

					enrolledCourses.add(course);
				}

			} catch (Exception e) {
				Logger.getLogger(LoginValidationService.class.getName()).log(
						Level.SEVERE, null, e);
			}

			EnrollmentDAO[] enrolledCourseArr = new EnrollmentDAO[enrolledCourses
					.size()];
			for (int i = 0; i < enrolledCourses.size(); i++) {
				enrolledCourseArr[i] = enrolledCourses.get(i);
			}
			return enrolledCourseArr;

		} else {
			return null;
		}
	}

	public boolean insertEnrollment(String[] selectedCourses, String loginId) {

		boolean isSuccess = false;
		// Obtain JDBC connection
		Connection con = getConnection();

		if (selectedCourses != null) {
			// Create sql transaction with insert query for each selected
			// course.
			try {
				for (int i = 0; i < selectedCourses.length; i++) {

					String insertEnrolmentSQL = "insert into student_portal.course_enrollment (course_id, registered_user_id) values ("
							+ selectedCourses[i]
							+ ", (select registered_user_id from student_portal.user_login where login_id = '"
							+ loginId + "'))";

					PreparedStatement stmt = con
							.prepareStatement(insertEnrolmentSQL);
					int affectedRows = stmt.executeUpdate();

					PracticeDAO[] practiceList = this
							.getPracticeListForCourse(selectedCourses[i]);
					if (practiceList != null) {
						for (int j = 0; j < practiceList.length; j++) {

							String insertPracticeSQL = "insert into student_portal.course_participation (registered_user_id, practice_id) values ((select registered_user_id from student_portal.user_login where login_id = '"
									+ loginId + "'), " + practiceList[j] + ")";

							PreparedStatement stmt2 = con
									.prepareStatement(insertPracticeSQL);
							int affectedPracticeRows = stmt2.executeUpdate();

							if (affectedPracticeRows <= 0)
								return false;
						}
					}

					if (affectedRows > 0)
						isSuccess = true;
					else
						return false;
				}

			} catch (Exception e) {
				Logger.getLogger(LoginValidationService.class.getName()).log(
						Level.SEVERE, null, e);
			}
		}
		return isSuccess;
	}

	public PrerequestCourseDAO[] getPrerequestCourses(int courseId) {

		List<PrerequestCourseDAO> preReqCourses = new ArrayList<PrerequestCourseDAO>();

		// Obtain JDBC connection
		Connection con = getConnection();
		// Create sql query
		String sqlQuery = "select * FROM student_portal.course_prerequest where course_id='"
				+ courseId + "'";

		if (con != null) {
			try {
				PreparedStatement stmt = con.prepareStatement(sqlQuery);
				ResultSet rs = stmt.executeQuery();

				if (rs == null)
					return null;

				while (rs.next()) {
					PrerequestCourseDAO preReqCourse = new PrerequestCourseDAO();
					preReqCourse.setPrereqId(rs.getInt(1));
					preReqCourse.setPrereqCourseId(rs.getInt(2));
					preReqCourse.setCourseId(rs.getInt(3));
					preReqCourse.setDescription(rs.getString(4));

					preReqCourses.add(preReqCourse);
				}

			} catch (Exception e) {
				Logger.getLogger(LoginValidationService.class.getName()).log(
						Level.SEVERE, null, e);
			}

			PrerequestCourseDAO[] preCoursesArr = new PrerequestCourseDAO[preReqCourses
					.size()];

			for (int i = 0; i < preReqCourses.size(); i++) {
				preCoursesArr[i] = preReqCourses.get(i);
			}

			return preCoursesArr;

		} else {
			return null;
		}
	}

	public PracticeDAO[] getPracticeListForCourse(String courseId) {

		List<PracticeDAO> practiceList = new ArrayList<PracticeDAO>();

		// Obtain JDBC connection
		Connection con = getConnection();
		// Create sql query
		String sqlQuery = "select * FROM student_portal.course_practice where course_id='"
				+ courseId + "'";

		if (con != null) {
			try {
				PreparedStatement stmt = con.prepareStatement(sqlQuery);
				ResultSet rs = stmt.executeQuery();

				if (rs == null)
					return null;

				while (rs.next()) {
					PracticeDAO practice = new PracticeDAO();
					practice.setPracticeId(rs.getInt(1));
					practice.setCourseId(rs.getInt(2));
					practice.setPracticeName(rs.getString(3));
					practice.setPracticeType(rs.getString(4));
					practice.setDueDate(rs.getString(5));
					practice.setDescription(rs.getString(6));

					practiceList.add(practice);
				}

			} catch (Exception e) {
				Logger.getLogger(LoginValidationService.class.getName()).log(
						Level.SEVERE, null, e);
			}

			PracticeDAO[] practiceArr = new PracticeDAO[practiceList.size()];

			for (int i = 0; i < practiceList.size(); i++) {
				practiceArr[i] = practiceList.get(i);
			}

			return practiceArr;

		} else {
			return null;
		}
	}

	public ClassStatDAO[] getClassStats(int courseId, String loginId) {

		List<ClassStatDAO> clsStatList = new ArrayList<ClassStatDAO>();
		// Obtain JDBC connection
		Connection con = getConnection();

		PracticeDAO[] practiceArr = this.getPracticeListForCourse(String
				.valueOf(courseId));

		if (practiceArr == null)
			return null;

		for (int i = 0; i < practiceArr.length; i++) {
			// Create sql query
			String clsStatQuery = "select pr.course_id, cp.practice_id, pr.name, max(cp.grade), min(cp.grade), avg(cp.grade) from student_portal.course_participation cp inner join student_portal.course_practice pr on cp.practice_id = pr.practice_id where pr.course_id = "
					+ courseId
					+ " and pr.practice_id = "
					+ practiceArr[i].getPracticeId();

			String userScoreQuery = "select grade from student_portal.course_participation "
					+ "where practice_id = "
					+ practiceArr[i].getPracticeId()
					+ " and registered_user_id = (select registered_user_id from student_portal.user_login where login_id='"
					+ loginId + "')";

			if (con != null) {
				try {
					PreparedStatement stmt = con.prepareStatement(clsStatQuery);
					ResultSet rs = stmt.executeQuery();

					ClassStatDAO item = new ClassStatDAO();

					if (rs == null)
						return null;
					while (rs.next()) {
						item.setCourseId(rs.getInt(1));
						item.setPracticeId(rs.getInt(2));
						item.setPracticeName(rs.getString(3));
						item.setMinScore(rs.getInt(4));
						item.setMaxScore(rs.getInt(5));
						item.setAvgScore(rs.getInt(6));
					}

					PreparedStatement stmt2 = con
							.prepareStatement(userScoreQuery);
					ResultSet rs2 = stmt2.executeQuery();

					if (rs2 == null)
						return null;
					while (rs2.next()) {
						item.setStudentScore(rs2.getInt(1));
					}
					clsStatList.add(item);

				} catch (Exception e) {
					Logger.getLogger(LoginValidationService.class.getName())
							.log(Level.SEVERE, null, e);
				}

			} else {
				return null;
			}
		}
		if ((clsStatList != null) && !(clsStatList.isEmpty())) {
			ClassStatDAO[] clsStatArr = new ClassStatDAO[clsStatList.size()];
			clsStatArr = (ClassStatDAO[]) clsStatList.toArray(clsStatArr);
			return clsStatArr;
		}
		return null;
	}

	/**
	 * Obtain the JDBC connection from the connection pool.
	 * 
	 * @return JDBC connection object.
	 */
	private Connection getConnection() {

		Context initContext = null;
		Connection con = null;

		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");

			DataSource ds = (DataSource) envContext
					.lookup("jdbc/student_portal");

			con = ds.getConnection();

		} catch (Exception e) {
			Logger.getLogger(LoginValidationService.class.getName()).log(
					Level.SEVERE, null, e);
		}

		return con;
	}
}

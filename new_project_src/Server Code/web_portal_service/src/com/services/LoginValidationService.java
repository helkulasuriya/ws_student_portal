package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.dao.PortalUserDAO;

/**
 * WS implementation for user account validation
 */
public class LoginValidationService {

	/**
	 * Validate the user against the database data, return the user information
	 * related to the provided user name and password.
	 * 
	 * @param userName
	 *            Login id of the user.
	 * @param password
	 *            Password of the user.
	 * @return PortalUserDAO object with data populated or NULL if user does not
	 *         exist.
	 */
	public PortalUserDAO validateUser(String userName, String password) {

		PortalUserDAO registeredUser = null;

		// Obtain JDBC connection
		Connection con = getConnection();
		// Create sql query
		String sqlQuery = "SELECT u.login_id, u.password, ru.name, ru.user_type, ru.user_contact_no, ru.user_address FROM user_login u INNER JOIN  registered_user ru ON u.registered_user_id=ru.registered_user_id WHERE login_id='"
				+ userName + "' AND password='" + password + "'";

		if (con != null) {
			try {
				PreparedStatement stmt = con.prepareStatement(sqlQuery);
				ResultSet rs = stmt.executeQuery();

				if (rs == null)
					return null;

				while (rs.next()) {
					registeredUser = new PortalUserDAO(
							rs.getString("login_id"), rs.getString("password"),
							rs.getString("name"),
							rs.getString("user_contact_no"),
							rs.getString("user_address"));
				}
				// Updating the user's last logged in time.
				boolean updateTimeStatus = this.updateUserLoginTime(userName);
				if (!updateTimeStatus)
					return null;

			} catch (Exception e) {
				Logger.getLogger(LoginValidationService.class.getName()).log(
						Level.SEVERE, "login validation", e);
			}

			return registeredUser;

		} else {
			return null;
		}
	}

	/**
	 * Changes the logged in user's password to the newly entered value.
	 * 
	 * @param userId
	 *            Logged in user id.
	 * @param newPW
	 *            New password value
	 * @return True if updated a single data row, else false.
	 */
	public boolean changePassword(String userId, String oldPW, String newPW) {
		boolean changeStatus = false;

		// Obtain JDBC connection
		Connection con = getConnection();
		// Create sql query
		String sqlQuery = "UPDATE user_login SET password='" + newPW
				+ "' WHERE login_id='" + userId + "' AND password='" + oldPW
				+ "'";

		if (con != null) {
			try {
				PreparedStatement stmt = con.prepareStatement(sqlQuery);
				int affectedRows = stmt.executeUpdate();

				if (affectedRows == 1) {
					changeStatus = true;
				}
			} catch (Exception e) {
				Logger.getLogger(LoginValidationService.class.getName()).log(
						Level.SEVERE, "change passwrd", e);
			}
		}
		return changeStatus;
	}

	/**
	 * Updates user logged in time, when user loggin in.
	 */
	private boolean updateUserLoginTime(String loginId) {

		boolean updateStatus = false;
		// Obtain JDBC connection
		Connection con = getConnection();

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = df.format(new Date());
		// Create sql query
		String sqlQuery = "update student_portal.user_login set last_login_time='"
				+ dateStr + "' where login_id = '" + loginId + "'";

		if (con != null) {
			try {
				PreparedStatement stmt = con.prepareStatement(sqlQuery);
				int affectedRows = stmt.executeUpdate();

				if (affectedRows == 1) {
					updateStatus = true;
				}
			} catch (Exception e) {
				Logger.getLogger(LoginValidationService.class.getName()).log(
						Level.SEVERE, "logged in time", e);
			}
		}
		return updateStatus;
	}

	/**
	 * Obtain the JDBC connection from the connection pool.
	 * 
	 * @return JDBC connection object.
	 */
	private Connection getConnection() {

		Context initContext = null;
		Connection con = null;

		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");

			DataSource ds = (DataSource) envContext
					.lookup("jdbc/student_portal");

			con = ds.getConnection();

		} catch (Exception e) {
			Logger.getLogger(LoginValidationService.class.getName()).log(
					Level.SEVERE, null, e);
		}

		return con;
	}
}

package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.dao.PortalUserDAO;
import com.util.Utils;

/**
 * WS implementation for user account validation
 */
public class CourseAttributeManager {
	//

	enum ATTRIBUTE_TYPE {
		DESCRIPTION, NOTE, SYLLABUS, ANNOUNCEMENT
	}

	public String obtainCourseResource(int attributeId, String attribType) {

		StringBuilder response = new StringBuilder("");

		if (attribType.equalsIgnoreCase(ATTRIBUTE_TYPE.DESCRIPTION.toString())) {
			System.out.println(ATTRIBUTE_TYPE.DESCRIPTION);
			response.append(this.loadCourseResources(attributeId,
					ATTRIBUTE_TYPE.DESCRIPTION));
		} else if (attribType.equalsIgnoreCase(ATTRIBUTE_TYPE.NOTE.toString())) {
			System.out.println(ATTRIBUTE_TYPE.NOTE);
			response.append(this.loadCourseResources(attributeId,
					ATTRIBUTE_TYPE.NOTE));
		} else if (attribType.equalsIgnoreCase(ATTRIBUTE_TYPE.SYLLABUS
				.toString())) {
			System.out.println(ATTRIBUTE_TYPE.SYLLABUS);
			response.append(this.loadCourseResources(attributeId,
					ATTRIBUTE_TYPE.SYLLABUS));
		} else if (attribType.equalsIgnoreCase(ATTRIBUTE_TYPE.ANNOUNCEMENT
				.toString())) {
			System.out.println(ATTRIBUTE_TYPE.ANNOUNCEMENT);
			response.append(this.loadCourseResources(attributeId,
					ATTRIBUTE_TYPE.ANNOUNCEMENT));
		} else {
			System.out.println("Invalid");
			response = new StringBuilder("");
		}

		return response.toString();
	}

	public String updateCourseResource(int attributeId, String content,
			int userId) {

		StringBuilder response = new StringBuilder("test");

		if (attributeId > 0 && userId > 0 && content != null) {
			System.out.println("Calling update");
			response.append(modifyCourseResources(attributeId, content, userId));
		} else {
			System.out.println("Invalid");
			response = new StringBuilder("");
		}

		return response.toString();
	}

	private boolean modifyCourseResources(int attributeId, String content,
			int userId) {

		boolean reslut = false;

		Context initContext;
		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext
					.lookup("jdbc/student_portal");
			Connection conn = ds.getConnection();

			String selectSQL = "UPDATE student_portal.course_attributes "
					+ "SET course_attributes.value =? ,"
					+ "last_modified =? , " + "created_by =?  "
					+ "WHERE 1=1 AND " + "course_attributes.attribute_id=? ";

			PreparedStatement preparedStatement = conn
					.prepareStatement(selectSQL);

			preparedStatement.setString(1, content);
			preparedStatement.setString(2, Utils.getInstance().getDateStamp());
			preparedStatement.setInt(3, userId);
			preparedStatement.setInt(4, attributeId);

			// execute select SQL stetement
			int count = preparedStatement.executeUpdate();

			if (count > 0)
				reslut = true;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return reslut;
	}

	private String loadCourseResources(int attributeId,
			ATTRIBUTE_TYPE attribType) {

		StringBuilder response = new StringBuilder("");

		Context initContext;
		try {
			initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext
					.lookup("jdbc/student_portal");
			Connection conn = ds.getConnection();

			String selectSQL = "SELECT "
					+ "course_attributes.value FROM student_portal.course_attributes "
					+ "WHERE 1=1 AND " + "course_attributes.attribute_id=? "
					+ "AND " + "course_attributes.attribute_type=?";

			System.out.println("xxxx " + selectSQL);
			PreparedStatement preparedStatement = conn
					.prepareStatement(selectSQL);

			preparedStatement.setInt(1, attributeId);
			preparedStatement.setString(2, attribType.toString());

			// execute select SQL stetement
			ResultSet rs = preparedStatement.executeQuery();

			if (rs != null && rs.next()) {
				response.append(rs.getString(1));

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response.toString();
	}
}

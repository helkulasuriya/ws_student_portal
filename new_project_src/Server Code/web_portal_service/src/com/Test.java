package com;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Test
{

    public Test()
    {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param args
     * @throws SQLException 
     */
    public static void main(String[] args) throws SQLException
    {
        // TODO Auto-generated method stub

        PreparedStatement preparedStatement  = null;
        

        
        Map<Integer,Object> str=new HashMap<Integer,Object>();
        
        str.put(1, new Integer(1232));
        str.put(2, new Float(1232));
        str.put(3, new Boolean(true));
        str.put(4, ("1232"));
        
        for(Integer index : str.keySet())
        {
            Object value=str.get(index);
            
            if(value==null)
                return;
            else if(value instanceof String)
                preparedStatement.setString(index, value.toString());
//                System.out.println(value+" S ");
            else if(value instanceof Float)
                preparedStatement.setFloat(index, Float.valueOf(value.toString()));                
//                System.out.println(value+" F ");
            else if(value instanceof Boolean)
                preparedStatement.setBoolean(index, Boolean.valueOf(value.toString()));               
//                System.out.println(value+" B ");
            else if(value instanceof Number)
                preparedStatement.setInt(index, Integer.valueOf(value.toString()));                
//                System.out.println(value+" S ");
            else if(value instanceof Byte[])
                preparedStatement.setBytes(index, (value.toString().getBytes()));                
//                System.out.println(value+" B[] ");
        }
        
        

        
        
    }

}

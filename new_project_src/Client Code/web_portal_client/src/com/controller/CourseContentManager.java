package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;

import services.stub.course.Web_portal_CourseContentManagerStub;
import services.stub.course.Web_portal_CourseContentManagerStub.CourseDAO;

import com.util.Utils;

/*
 *  Description: Worked as a controller to manipulate the content of the course elements. 
 *  Methods used: POST, GET
 */
public class CourseContentManager extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = null;

	public enum messageType {
		ERROR, SUCCESS
	}

	/**
	 * Default constrouctor
	 */
	public CourseContentManager() {
		super();
		logger = Utils.intLog4J(this.getClass());
	}

	/**
	 * Use to receive requests over GET
	 * 
	 * @param request
	 *            HttpServletRequest : The currency type where it has to be
	 *            converted from.
	 * @param response
	 *            HttpServletResponse : The currency type where it has to be
	 *            converted to.
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		boolean isRequiredFilesMissing = false;

		CourseDAO course = new CourseDAO();
		PrintWriter writer = response.getWriter();

		int courseId = 0;
		int practiceId = 0;
		String percaetage = "";

		if (request.getParameter("course_id") == null
				&& request.getParameter("course_id").trim().length() == 0) {
			isRequiredFilesMissing = true;
			writer.write("ERROR : Course id is required");
		} else
			practiceId = Integer.valueOf(request.getParameter("course_id"));

		if (request.getParameter("course_date") == null
				&& request.getParameter("course_date").trim().length() == 0) {
			isRequiredFilesMissing = true;
			writer.write("ERROR : Course date is required");
		}

		if (request.getParameter("practice_id") == null
				&& request.getParameter("practice_id").trim().length() == 0) {
			isRequiredFilesMissing = true;
			writer.write("ERROR : Course practice id is required");
		} else
			courseId = Integer.valueOf(request.getParameter("practice_id"));

		if (request.getParameter("percentage_val") == null
				&& request.getParameter("percentage_val").trim().length() == 0) {
			isRequiredFilesMissing = true;
			writer.write("ERROR : Course percentage is required");
		} else
			percaetage = request.getParameter("percentage_val");

		if (!isRequiredFilesMissing && courseId > 0 && practiceId > 0) {

			if (updateCourseDate(practiceId, courseId,
					request.getParameter("course_date"), percaetage)) {
				writer.write("SUCCESS : Course information successfully updated!");
			} else {
				writer.write("ERROR : Error while updating course information.!");
			}

		}

	}

	/**
	 * Use to receive requests over POST
	 * 
	 * @param request
	 *            HttpServletRequest : The currency type where it has to be
	 *            converted from.
	 * @param response
	 *            HttpServletResponse : The currency type where it has to be
	 *            converted to.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		CourseDAO course = new CourseDAO();

		boolean isRequiredFilesMissing = false;

		if (request.getParameter("course_id") != null
				&& request.getParameter("course_id").trim().length() > 0)
			course.setCourseId(Integer.valueOf(request
					.getParameter("course_id")));
		else {
			isRequiredFilesMissing = true;
			request.getSession().setAttribute("message",
					"Course ID is required");
		}

		if (request.getParameter("course_title") != null
				&& request.getParameter("course_title").trim().length() > 0)
			course.setName(request.getParameter("course_title"));
		else {
			isRequiredFilesMissing = true;
			request.getSession().setAttribute("message",
					"Course Title is required");
		}

		if (request.getParameter("course_location") != null
				&& request.getParameter("course_location").trim().length() > 0)
			course.setLocation(request.getParameter("course_location"));
		else {
			isRequiredFilesMissing = true;
			request.getSession().setAttribute("message",
					"Course Location is required");
		}

		if (request.getParameter("course_term") != null
				&& request.getParameter("course_term").trim().length() > 0)
			course.setTerm(request.getParameter("course_term"));
		else {
			isRequiredFilesMissing = true;
			request.getSession().setAttribute("message",
					"Course Tearm is required");
		}

		if (request.getParameter("course_credits") != null
				&& request.getParameter("course_credits").trim().length() > 0)
			course.setCredit(request.getParameter("course_credits"));
		else {
			isRequiredFilesMissing = true;
			request.getSession().setAttribute("message",
					"Course credit is required");
		}

		if (request.getParameter("course_time") != null
				&& request.getParameter("course_time").trim().length() > 0)
			course.setTime(request.getParameter("course_time"));

		if (isRequiredFilesMissing) {
			request.getSession().setAttribute("messageType",
					messageType.ERROR.toString());
			request.getRequestDispatcher("template/message.jsp").forward(
					request, response);
		} else {

			if (updateCourse(course)) {
				request.getSession().setAttribute("messageType",
						messageType.SUCCESS.toString());
				request.getSession().setAttribute(
						"message",
						"Course : " + course.getName()
								+ " successfully updated.");

			} else {
				request.getSession().setAttribute("messageType",
						messageType.ERROR.toString());
				request.getSession().setAttribute(
						"message",
						"Error while updating course : " + course.getName()
								+ ".");

			}

		}
		response.sendRedirect("template/message.jsp");
	}

	/**
	 * Use to update the course date/percentage contnet based on id
	 * 
	 * @param practiceId
	 *            String : percentage id
	 * @param courseId
	 *            String : course id
	 * @param date
	 *            String : practice date
	 * @param date
	 *            percentage : percentage for the practice item
	 */
	private boolean updateCourseDate(int practiceId, int courseId, String date,
			String percaetage) {

		boolean response = false;
		Web_portal_CourseContentManagerStub stub = null;
		ConfigurationContext ctx = getConfigurationContext();

		if (ctx == null)
			return response;

		try {

			stub = new Web_portal_CourseContentManagerStub(ctx, Utils
					.getInstance().getProperty("wsdl.course.content.uri"));

			response = stub.updateCourseData(courseId, practiceId, date,
					percaetage);
			if (!response)
				logger.info("Calling webservice :CourseContentManager was unsuessfull.");

		}

		catch (AxisFault e) {
			logger.error("Axis Fault while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (RemoteException e) {
			logger.error("Remote Exception while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("Error while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		}
		return response;

	}

	/**
	 * Use to update the course date/percentage contnet based on CourseDAO
	 * object
	 * 
	 * @param CourseDAO
	 *            course : CourseDAO object
	 */
	private boolean updateCourse(CourseDAO course) {

		boolean response = false;
		Web_portal_CourseContentManagerStub stub = null;
		ConfigurationContext ctx = getConfigurationContext();

		if (ctx == null) {
			return response;
		}

		try {

			stub = new Web_portal_CourseContentManagerStub(ctx, Utils
					.getInstance().getProperty("wsdl.course.content.uri"));

			response = stub.updateCourse(course);

			logger.info("Calling webservice :CourseContentManager by user ");

		}

		catch (AxisFault e) {
			logger.error("Axis Fault while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (RemoteException e) {
			logger.error("Remote Exception while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("Error while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		}
		return response;

	}

	/**
	 * Obtain the configuration context of the application, in order to encrypt
	 * out going messages and provide WS security.
	 * 
	 * @return configuration context object
	 */
	private ConfigurationContext getConfigurationContext() {

		ConfigurationContext ctx = null;
		try {
			ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client",
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client\\client.axis2.xml");
		} catch (AxisFault e) {

			e.printStackTrace();
		}

		return ctx;
	}
}

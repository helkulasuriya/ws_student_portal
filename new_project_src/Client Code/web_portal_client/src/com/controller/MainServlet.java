package com.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dao.EnrollmentClientDAO;
import com.dao.SystemUser;
import com.facade.IEnrollmentFacade;
import com.facade.impl.EnrollmentFacade;
import com.util.Utils;

/*
 *  Description: Worked as the initial controler that redirect the user based on type. 
 *  Methods used:  GET,POST
 */
public final class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	static Logger logger = null;

	private IEnrollmentFacade enrollmentFacade = null;
	SystemUser loggedInUser = null;

	/**
	 * Default constrouctor
	 */
	public MainServlet() {
		super();
		logger = Utils.intLog4J(this.getClass());
		enrollmentFacade = new EnrollmentFacade();
	}

	/**
	 * Use to receive requests over GET
	 * 
	 * @param request
	 *            HttpServletRequest : The currency type where it has to be
	 *            converted from.
	 * @param response
	 *            HttpServletResponse : The currency type where it has to be
	 *            converted to.
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		logger.info("Hit the GET method.");
		String nextJSP = request.getParameter("request");

		if (nextJSP.trim().equalsIgnoreCase("/test.jsp")) {
			request.getRequestDispatcher(nextJSP).forward(request, response);
			return;
		} else if (nextJSP.trim().equalsIgnoreCase("/viewMainContent.jsp")) {
			request.getRequestDispatcher(nextJSP).forward(request, response);
			return;
		} else if (nextJSP.trim().equalsIgnoreCase("register")) {
			response.sendRedirect("studentContent/course_search.jsp");
			return;

		} else if (nextJSP.trim().equalsIgnoreCase("logout")) {
			request.getSession().invalidate();
			response.sendRedirect("login.jsp");
			return;
		}

		logger.warn("Request forwared.");
	}

	/**
	 * Use to receive requests over POST
	 * 
	 * @param request
	 *            HttpServletRequest : The currency type where it has to be
	 *            converted from.
	 * @param response
	 *            HttpServletResponse : The currency type where it has to be
	 *            converted to.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		logger.info("Hit the GET method.");
		String nextJSP = request.getParameter("request");
		HttpSession session = request.getSession();

		loggedInUser = (SystemUser) (session).getAttribute("loggedInUser");

		if (nextJSP.trim().equalsIgnoreCase("enroll")) {
			// Enrolling the student for courses.
			String[] successMsg = this.enrollForCourses(request, session);
			if (successMsg != null) {
				// Setting result message details to session.
				String messageType = successMsg[0].equals("true") ? "SUCCESS"
						: "ERROR";
				session.setAttribute("messageType", messageType);
				session.setAttribute("message", successMsg[1]);

				// Renew session variables if insert successful.
				if (successMsg[0].equals("true")) {
					logger.info("it is true");
					this.setCourseListSession(session);
				}
			}
			response.sendRedirect("../template/message.jsp");
			return;

		} else if (nextJSP.trim().equalsIgnoreCase("search")) {

			response.sendRedirect("studentContent/course_reg_list.jsp?searchTxt="
					+ request.getParameter("searchTxt")
					+ "&searchCriteria="
					+ request.getParameter("searchCriteria"));
			return;
		}

	}

	/**
	 * Use to set the invalid login attemps
	 * 
	 * @param session
	 *            HttpSession : session object
	 * @param request
	 *            HttpServletRequest :requet object
	 */
	private String[] enrollForCourses(HttpServletRequest request,
			HttpSession session) {

		String[] selectedCourses = request
				.getParameterValues("selectedCourses");
		EnrollmentClientDAO[] enrolledCourses = (EnrollmentClientDAO[]) session
				.getAttribute("enrolledCourses");

		String userName = loggedInUser == null ? null : (loggedInUser
				.getUserName());
		String[] msgArr = enrollmentFacade.enrollForCourses(selectedCourses,
				enrolledCourses, userName);

		return msgArr;
	}

	private void setCourseListSession(HttpSession session) {
		String userName = loggedInUser == null ? null : (loggedInUser
				.getUserName());
		EnrollmentClientDAO[] newCourseArr = enrollmentFacade
				.getEnrolledCourses(userName);

		if (newCourseArr != null) {
			// Set the courses array and, current number of courses to session.
			session.setAttribute("enrolledCourses", newCourseArr);
			session.setAttribute("numOfCourses", newCourseArr.length);
		}
	}

}

package com.controller;

/*
 *  Description: Worked as a controller to manipulate Attributes of the course content. 
 *  Methods used: POST
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;

import services.stub.course.Web_portal_CourseAttributeManagerStub;

import com.util.*;

public class AttributeReader extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private Logger logger = null;

	/**
	 * Default constrouctor
	 */
	public AttributeReader() {
		super();
		logger = Utils.intLog4J(this.getClass());
	}

	/**
	 * Use to receive requests over POST
	 * 
	 * @param request
	 *            HttpServletRequest : The currency type where it has to be
	 *            converted from.
	 * @param response
	 *            HttpServletResponse : The currency type where it has to be
	 *            converted to.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		PrintWriter writer = response.getWriter();

		if (request.getParameter("operation") != null) {

			if (request.getParameter("operation").equalsIgnoreCase("get")
					&& request.getParameter("object_id") != null
					&& request.getParameter("user_id") != null
					&& request.getParameter("attrib_type") != null) {

				writer.write(readAttribute(request.getParameter("object_id"),
						request.getParameter("user_id"),
						request.getParameter("attrib_type")));
			} else if (request.getParameter("operation").equalsIgnoreCase(
					"update")
					&& request.getParameter("object_id") != null
					&& request.getParameter("user_id") != null
					&& request.getParameter("content") != null) {

				writer.write(updateAttribute(request.getParameter("object_id"),
						request.getParameter("user_id"),
						request.getParameter("content")));

			} else
				writer.write("Invalid!");

		}

	}

	/**
	 * Use to update the attributes based on id
	 * 
	 * @param objectId
	 *            String : attribute id
	 * @param userId
	 *            String : user id
	 * @param content
	 *            String : content of the attribute
	 */
	private String updateAttribute(String objectId, String userId,
			String content) {

		StringBuilder responseString = new StringBuilder();
		Web_portal_CourseAttributeManagerStub stub = null;
		ConfigurationContext ctx = getConfigurationContext();

		if (ctx == null) {
			return null;
		}

		try {

			stub = new Web_portal_CourseAttributeManagerStub(ctx, Utils
					.getInstance().getProperty("wsdl.course.attribute.uri"));
			int attribId = Integer.valueOf(objectId);
			int intUserId = Integer.valueOf(userId);

			logger.info("Calling webservice : CourseAttributeManager by user "
					+ userId);

			responseString.append(stub.updateCourseResource(attribId, content,
					intUserId));
			System.out.println("Calling webservice : "
					+ responseString.toString());

		}

		catch (AxisFault e) {
			logger.error("Axis Fault while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (RemoteException e) {
			logger.error("Remote Exception while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("Error while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		}
		return responseString.toString();

	}

	/**
	 * Use to read the attributes based on id & type
	 * 
	 * @param objectId
	 *            String : attribute id
	 * @param userId
	 *            String : user id
	 * @param attribType
	 *            String : attribute type
	 */
	private String readAttribute(String objectId, String userId,
			String attribType) {

		StringBuilder responseString = new StringBuilder();
		Web_portal_CourseAttributeManagerStub stub = null;

		ConfigurationContext ctx = getConfigurationContext();

		if (ctx == null) {
			return null;
		}
		try {

			stub = new Web_portal_CourseAttributeManagerStub(ctx, Utils
					.getInstance().getProperty("wsdl.course.attribute.uri"));
			int attribId = Integer.valueOf(objectId);

			logger.info("Calling webservice : CourseAttributeManager by user "
					+ userId);

			responseString.append(stub.obtainCourseResource(attribId,
					attribType));
			System.out.println("Calling webservice : "
					+ responseString.toString());

		}

		catch (AxisFault e) {
			logger.error("Axis Fault while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (RemoteException e) {
			logger.error("Remote Exception while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("Error while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		}
		return responseString.toString();

		/**
	 * Obtain the configuration context of the application, in order to encrypt
	 * out going messages and provide WS security.
	 * 
	 * @return configuration context object
	 */
		private ConfigurationContext getConfigurationContext() {

		ConfigurationContext ctx = null;
		try {
			ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client",
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client\\client.axis2.xml");
		} catch (AxisFault e) {

			e.printStackTrace();
		}

		return ctx;
	}
	}
}

package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;

import services.stub.course.Web_portal_CourseContentManagerStub;
import services.stub.course.Web_portal_CourseContentManagerStub.CourseDAO;

import com.util.Utils;

/*
 *  Description: Worked as a controller to manipulate the GPA value of studes 
 *  Methods used:  GET
 */
public class GPACalculator extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = null;

	/**
	 * Default constrouctor
	 */
	public GPACalculator() {
		super();
		logger = Utils.intLog4J(this.getClass());
	}

	/**
	 * Use to receive requests over GET
	 * 
	 * @param request
	 *            HttpServletRequest : The currency type where it has to be
	 *            converted from.
	 * @param response
	 *            HttpServletResponse : The currency type where it has to be
	 *            converted to.
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		boolean isRequiredFilesMissing = false;

		CourseDAO course = new CourseDAO();
		PrintWriter writer = response.getWriter();

		int courseId = 0;
		int studentId = 0;
		float gpa = 0;

		if (request.getParameter("course_id") == null
				&& request.getParameter("course_id").trim().length() == 0) {
			isRequiredFilesMissing = true;
			writer.write("ERROR : Course id is required");
		} else
			courseId = Integer.valueOf(request.getParameter("course_id"));

		if (request.getParameter("student_id") == null
				&& request.getParameter("student_id").trim().length() == 0) {
			isRequiredFilesMissing = true;
			writer.write("ERROR : Student ID is required");
		} else
			studentId = Integer.valueOf(request.getParameter("student_id"));

		if (request.getParameter("student_gpa") == null
				&& request.getParameter("student_gpa").trim().length() == 0) {
			isRequiredFilesMissing = true;
			writer.write("ERROR : Student GPA is required");
		} else
			gpa = Integer.valueOf(request.getParameter("student_gpa"));

		if (!isRequiredFilesMissing && courseId > 0 && studentId > 0) {

			if (updateGPA(courseId, studentId, gpa)) {
				writer.write("SUCCESS : Course information successfully updated!");
			} else {
				writer.write("ERROR : Error while updating course information.!");
			}

		}

	}

	/**
	 * Use to update the student GPA
	 * 
	 * @param studentId
	 *            String : studnet id
	 * @param courseId
	 *            String : course id
	 * @param gpa
	 *            float :GPA value
	 */
	private boolean updateGPA(int courseId, int studentId, float gpa) {

		boolean response = false;
		Web_portal_CourseContentManagerStub stub = null;

		ConfigurationContext ctx = getConfigurationContext();

		if (ctx == null)
			return null;

		try {

			stub = new Web_portal_CourseContentManagerStub(ctx, Utils
					.getInstance().getProperty("wsdl.course.content.uri"));

			response = stub.updateGPA(courseId, studentId, gpa);
			if (!response)
				logger.info("Calling webservice :CourseContentManager was unsuessfull.");

		}

		catch (AxisFault e) {
			logger.error("Axis Fault while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (RemoteException e) {
			logger.error("Remote Exception while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("Error while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		}
		return response;

	}

	/**
	 * Obtain the configuration context of the application, in order to encrypt
	 * out going messages and provide WS security.
	 * 
	 * @return configuration context object
	 */
	private ConfigurationContext getConfigurationContext() {

		ConfigurationContext ctx = null;
		try {
			ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client",
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client\\client.axis2.xml");
		} catch (AxisFault e) {

			e.printStackTrace();
		}

		return ctx;
	}

}

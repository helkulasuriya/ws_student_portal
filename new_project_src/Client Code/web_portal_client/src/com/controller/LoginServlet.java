package com.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.EnrollmentClientDAO;
import com.dao.SystemUser;
import com.facade.IEnrollmentFacade;
import com.facade.ILoginFacade;
import com.facade.impl.EnrollmentFacade;
import com.facade.impl.LoginFacade;

/*
 *  Description: Worked as a controller to mange user login.
 *  Methods used:  GET, POST
 */
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private ILoginFacade loginFacade = null;
	private IEnrollmentFacade enrollmentFacade = null;
	private SystemUser loggedInUser = null;

	public LoginServlet() {
		super();
		loginFacade = new LoginFacade();
		enrollmentFacade = new EnrollmentFacade();
	}

	/**
	 * Use to receive requests over GET
	 * 
	 * @param request
	 *            HttpServletRequest : The currency type where it has to be
	 *            converted from.
	 * @param response
	 *            HttpServletResponse : The currency type where it has to be
	 *            converted to.
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		super.doGet(req, resp);
	}

	/**
	 * Use to receive requests over POST
	 * 
	 * @param request
	 *            HttpServletRequest : The currency type where it has to be
	 *            converted from.
	 * @param response
	 *            HttpServletResponse : The currency type where it has to be
	 *            converted to.
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// Access the user inserted credentials.
		String userId = req.getParameter("username");
		String password = req.getParameter("password");

		HttpSession session = req.getSession(true); // Obtain session.

		// Validate user.
		boolean isValid = loginFacade.validateUser(userId, password,
				req.getRemoteHost());
		// Forward user to appropriate page.
		this.validateAndForwardUser(isValid, session, req, resp);

	}

	/**
	 * Use to validate the user and forward to the correct home page
	 * 
	 * @param isValid
	 *            boolean : studnet id
	 * @param session
	 *            HttpSession : course id
	 * @param req
	 *            HttpServletRequest :GPA value
	 * @param resp
	 *            HttpServletResponse :GPA value
	 */
	private void validateAndForwardUser(boolean isValid, HttpSession session,
			HttpServletRequest req, HttpServletResponse resp) {

		try {
			if (isValid) {
				// If login is valid set logged in user object to session.
				loggedInUser = loginFacade.getRegisteredUser();
				session.setAttribute("loggedInUser", loggedInUser);
				// Removing the login attempts from session, after successful
				// login.
				session.removeAttribute("loginAttempts");
				// Retrieve attempted login value.
				this.retrieveEnrolledCourses(session, loggedInUser);

				// Redirect the user to appropriate home page.
				if (loggedInUser.getUserType().equalsIgnoreCase(
						ILoginFacade.USER_TYPE.TEACHER.toString()))
					resp.sendRedirect("./teacherContent/teacher_home.jsp");
				else
					resp.sendRedirect("./studentContent/student_home.jsp");

			} else {
				this.setLoginAttempts(session);
				// Redirect the user back to login page.
				resp.sendRedirect("./login.jsp");
			}
		} catch (Exception e) {
			// Utils.intLog4J(getClass()).error("Error in fowarding request.",
			// e);
			e.printStackTrace();
		}
	}

	/**
	 * Use to set the invalid login attemps
	 * 
	 * @param session
	 *            HttpSession : course id
	 */
	private Integer setLoginAttempts(HttpSession session) {
		// Retrieve attempted login value.
		Integer loginAttempts = (Integer) session.getAttribute("loginAttempts");

		// Set login attempts to session.
		if (session.getAttribute("loginAttempts") != null) {
			loginAttempts += 1;
			session.setAttribute("loginAttempts", loginAttempts);
		} else {
			loginAttempts = new Integer(1);
			session.setAttribute("loginAttempts", loginAttempts);
		}
		return loginAttempts;
	}

	/**
	 * Use to set the retrieve enrolled courses
	 * 
	 * @param session
	 *            HttpSession : session object
	 * @param loggedInUser
	 *            SystemUser : logged In User id
	 */
	private void retrieveEnrolledCourses(HttpSession session,
			SystemUser loggedInUser) {
		// Retrieve enrolled courses of the logged in user.
		EnrollmentClientDAO[] enrolledCourses = enrollmentFacade
				.getEnrolledCourses(loggedInUser.getUserName());

		if (enrolledCourses != null) {
			// Set the courses array and, current number of courses to session.
			session.setAttribute("enrolledCourses", enrolledCourses);
			session.setAttribute("numOfCourses", enrolledCourses.length);
		}
	}

}

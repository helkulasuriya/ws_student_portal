package com.util;

/*
 * Student ID : s3397469 CourseDAO Code : COSC2278/2279 Assignment : 1 Description: A Utils class to share common
 * functionalities
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/*
 * Implemented as a singleton
 */
public class Utils
{

    private static Utils utils = null;
    private Logger utilLogger = null;
    Properties propertes = new Properties();

    
    
    
    
    
    
    
    

    private Utils()
    {
        utilLogger = intLog4J(this.getClass());
        initProperties();
        initWSSecurity();
    }
    
    private void initWSSecurity() {
    	utilLogger.info("Configuring WS Trust store for SSL");
    	
    	System.setProperty("javax.net.ssl.trustStore",  this.getProperty("ssl.truststore"));
    	System.setProperty("trustStorePassword", this.getProperty("ssl.turststore.password"));
		
	}

	public static Utils getInstance()
    {
        if (utils == null)
            utils = new Utils();

        return utils;
    }    

    /**
     *  Use to provide the Logger based on each class
     * @param classForLog Class : Used to specify the class 
     * @return Logger : a log4j logger 
     */
    public static Logger intLog4J(Class classForLog)
    {
        final Logger logger = Logger.getLogger(classForLog);
        PropertyConfigurator.configure("log4j.properties");
        return logger;
    }

    /**
     *  Used to initialize common properties
     */    
    private void initProperties()
    {
        utilLogger.info("Reading the default properties file: " + "build.properties");

        File propertiesFile = new File("build.properties");

        InputStream in = null;
        try
        {
        	if(propertiesFile==null || !propertiesFile.exists())
        	 in = this.getClass().getClassLoader()  
                     .getResourceAsStream("build.properties");  
        	else
        		in=new FileInputStream(propertiesFile);
        	
            propertes.load(in);
            in.close();
            utilLogger.info("Default properties loaded successfully.");
        }
        catch (FileNotFoundException e)
        {
            utilLogger.error("Error while loading the default properties file: build.properties, File Not Found",e);
        }
        catch (IOException e)
        {
            utilLogger.error("Error while loading the default properties file: build.properties",e);

        }
    }

    /**
     *  Use to obtain the values from build.properties 
     * @param key String : property key to obtain the value
     * @return String : value
     */
    public String getProperty(String key)
    {
        return propertes.getProperty(key);
    }

    
    public String formatDate(String rowDate){

    	String formattedDate="";
    	
    	DateFormat rowFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
    	DateFormat finalFormat = new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss");
    	Date date = null;
    	try
    	{
    	    date = rowFormat.parse( rowDate );
    	    formattedDate= finalFormat.format( date );
    	}
    	catch ( ParseException e )
    	{
    	        e.printStackTrace();
    	}

    		

    	return formattedDate;
    }
}

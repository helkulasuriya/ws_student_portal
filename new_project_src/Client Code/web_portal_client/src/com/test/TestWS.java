package com.test;

import java.rmi.RemoteException;

import javax.servlet.http.HttpSession;

import org.apache.axis2.AxisFault;

import services.stub.course.Web_portal_CourseContentManagerStub;

import com.facade.impl.LoginFacade;
import com.util.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TestWS
{

    public TestWS()
    {
  

		
        
    }


    /**
     * @param args
     * @throws ParseException 
     */
    public static void main(String[] args) throws ParseException
    {
    	TestWS t=new TestWS();


//    	SimpleDateFormat outFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

//    	outFormatter.format("2013-08-01 23:01:16");
    	
//    	String string = "2013-08-01";
//    	Date date = new SimpleDateFormat("yyMMddHHmm").parse("2013-08-01 23:01:16");
//    	System.out.println(date); // Sat Jan 02 00:00:00 BOT 2010
//    	
//    	System.setProperty("javax.net.ssl.trustStore", "C:/Program Files/Java/jdk1.6.0_37/jre/lib/security/localhost.truststore");
//    	System.setProperty("trustStorePassword", "123abc");
//    	updateCourseDate(2,1,"2013-06-01 23:01:16");

    	
 
    }
    
	private static boolean updateCourseDate(int practiceId, int courseId, String date) {

		boolean response = false;
		Web_portal_CourseContentManagerStub stub = null;

		try {

			stub = new Web_portal_CourseContentManagerStub(Utils
					.getInstance().getProperty("ssl.wsdl.course.content.uri"));


			response=stub.updateCourseData(courseId, practiceId, date,"80%");
			
			System.out.println("Calling webservice :CourseContentManager " +response);


		}

		catch (AxisFault e) {
			System.out.println("Axis Fault while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (RemoteException e) {
			System.out.println("Remote Exception while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error while calling the ADB webservice : "
					+ e.getMessage());
			e.printStackTrace();
		}
		return response;

	}
	

}

package com.dao;

import java.io.Serializable;

/**
 * DAO for course information
 */
public class CourseDAO implements Serializable {

	private static final long serialVersionUID = 1L;
	private int courseId;
	private String location;
	private String name;
	private String credit;
	private String time;
	private String term;

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public CourseDAO() {
		// TODO Auto-generated constructor stub
	}

}

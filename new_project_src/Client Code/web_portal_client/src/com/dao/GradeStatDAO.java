package com.dao;

import java.io.Serializable;

import services.stub.student.Web_portal_StudentActivityServiceStub.ClassStatDAO;

/**
 * Class to handle information regarding class statistics of student marks.
 */
public class GradeStatDAO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int courseId;
	private int practiceId;
	private String practiceName;
	private int minScore;
	private int maxScore;
	private int avgScore;
	private int studentScore;

	public GradeStatDAO() {
	}

	public GradeStatDAO(ClassStatDAO classStatDAO) {
		this.courseId = classStatDAO.getCourseId();
		this.practiceId = classStatDAO.getPracticeId();
		this.practiceName = classStatDAO.getPracticeName();
		this.minScore = classStatDAO.getMinScore();
		this.maxScore = classStatDAO.getMaxScore();
		this.avgScore = classStatDAO.getAvgScore();
		this.studentScore = classStatDAO.getStudentScore();
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public int getPracticeId() {
		return practiceId;
	}

	public void setPracticeId(int practiceId) {
		this.practiceId = practiceId;
	}

	public String getPracticeName() {
		return practiceName;
	}

	public void setPracticeName(String practiceName) {
		this.practiceName = practiceName;
	}

	public int getMinScore() {
		return minScore;
	}

	public void setMinScore(int minScore) {
		this.minScore = minScore;
	}

	public int getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(int maxScore) {
		this.maxScore = maxScore;
	}

	public int getAvgScore() {
		return avgScore;
	}

	public void setAvgScore(int avgScore) {
		this.avgScore = avgScore;
	}

	public int getStudentScore() {
		return studentScore;
	}

	public void setStudentScore(int studentScore) {
		this.studentScore = studentScore;
	}
}

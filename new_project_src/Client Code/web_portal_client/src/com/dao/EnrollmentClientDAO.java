package com.dao;

import java.io.Serializable;

import services.stub.student.Web_portal_StudentActivityServiceStub.EnrollmentDAO;

/**
 * Client DAO to handle enrollment information.
 */
public class EnrollmentClientDAO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int enrollmentId;
	private int studentId;
	private int courseId;
	private String courseName;
	private String courseTime;
	private String finalGrade;
	private String credit;

	public EnrollmentClientDAO() {
	}

	/*
	 * Copies the values of stub enrollment object to the client object.
	 */
	public EnrollmentClientDAO(EnrollmentDAO enrollmentDAO) {
		this.enrollmentId = enrollmentDAO.getEnrollmentId();
		this.studentId = enrollmentDAO.getStudentId();
		this.courseId = enrollmentDAO.getCourseId();
		this.courseName = enrollmentDAO.getCourseName();
		this.courseTime = enrollmentDAO.getCourseTime();
		this.finalGrade = enrollmentDAO.getFinalGrade();
		this.credit = enrollmentDAO.getCredit();
	}

	public int getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(int enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseTime() {
		return courseTime;
	}

	public void setCourseTime(String courseTime) {
		this.courseTime = courseTime;
	}

	public String getFinalGrade() {
		return finalGrade;
	}

	public void setFinalGrade(String finalGrade) {
		this.finalGrade = finalGrade;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}
}

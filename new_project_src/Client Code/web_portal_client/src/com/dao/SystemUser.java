package com.dao;

import java.io.Serializable;
import java.util.Date;

import com.facade.ILoginFacade;

import services.stub.login.Web_portal_LoginValidationServiceStub.PortalUserDAO;

/**
 * DAO class to handle the properies of logged in user.
 */
public class SystemUser implements Serializable {

	private static final long serialVersionUID = 1L;
	private String userName;
	private String userFullName;

	private String userType;
	private String userIPaddress;
	private Date userLoginTime;

	public SystemUser() {
	}

	public SystemUser(PortalUserDAO user, String IPAddr) {
		this.userName = user.getUserLoginName();
		this.userFullName = user.getName();
		setUserType();
		this.userIPaddress = IPAddr;
		this.userLoginTime = new Date();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
		setUserType();
	}

	public String getUserType() {
		return userType;
	}

	private void setUserType() {
		if (this.userName.startsWith("s"))
			this.userType = ILoginFacade.USER_TYPE.STUDENT.toString();
		else
			this.userType = ILoginFacade.USER_TYPE.TEACHER.toString();
	}

	public String getUserIPaddress() {
		return userIPaddress;
	}

	public void setUserIPaddress(String userIPaddress) {
		this.userIPaddress = userIPaddress;
	}

	public Date getUserLoginTime() {
		return userLoginTime;
	}

	public void setUserLoginTime(Date userLoginTime) {
		this.userLoginTime = userLoginTime;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		result = prime * result
				+ ((userType == null) ? 0 : userType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemUser other = (SystemUser) obj;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		if (userType == null) {
			if (other.userType != null)
				return false;
		} else if (!userType.equals(other.userType))
			return false;
		return true;
	}

	/* Overriding toString() implementation to provide custom messages. */
	@Override
	public String toString() {

		StringBuilder welcomeMessage = new StringBuilder();

		if (this.userType.equalsIgnoreCase(ILoginFacade.USER_TYPE.STUDENT
				.toString()))
			welcomeMessage
					.append("<img id=\"avc_logo_image\" src=\"../img/student.JPG\" >");
		else
			welcomeMessage
					.append("<img id=\"avc_logo_image\" src=\"../img/prof.JPG\" >");

		// <img id="avc_logo_image" src="../img/prof.JPG" style=" ">

		welcomeMessage.append(" &nbsp; Welcome " + this.userFullName);
		return welcomeMessage.toString();
	}

}

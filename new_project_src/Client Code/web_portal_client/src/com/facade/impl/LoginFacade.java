package com.facade.impl;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;

import services.stub.login.Web_portal_LoginValidationServiceStub;
import services.stub.login.Web_portal_LoginValidationServiceStub.PortalUserDAO;

import com.dao.SystemUser;
import com.facade.ILoginFacade;
import com.util.Utils;

/**
 * Facade class to communicate with the login service and perform user login
 * related functionality.
 */
public class LoginFacade implements ILoginFacade {

	private SystemUser registeredUser = null;

	/**
	 * Validates the user when loggin in.
	 */
	@Override
	public boolean validateUser(String userName, String password, String IP) {

		boolean isValid = this.getUserObject(userName, password, IP);
		return isValid;
	}

	/**
	 * Returns the current logged in user instance.
	 */
	@Override
	public SystemUser getRegisteredUser() {
		return registeredUser;
	}

	/**
	 * Handle functionality related to changing password. Communicates with the
	 * service to change password.
	 */
	@Override
	public boolean changePassword(String loginId, String oldPassword,
			String newPassword) {

		boolean changeStatus = false;

		try {
			ConfigurationContext ctx = getConfigurationContext();

			if (ctx == null)
				return changeStatus;

			Web_portal_LoginValidationServiceStub stub = new Web_portal_LoginValidationServiceStub(
					ctx, Utils.getInstance().getProperty("wsdl.login.uri"));
			changeStatus = stub.changePassword(loginId, oldPassword,
					newPassword);

		} catch (Exception e) {
			// utilLogger.error("Error in accessing the web service", e);
			e.printStackTrace();
		}
		return changeStatus;
	}

	/**
	 * Communicates with the web service to obtain the valid user object, when
	 * provided the login user name and password.
	 */
	private boolean getUserObject(String userName, String password, String IP) {
		boolean isValid = false;

		try {
			ConfigurationContext ctx = getConfigurationContext();

			if (ctx == null) {
				return isValid;
			}

			Web_portal_LoginValidationServiceStub stub = new Web_portal_LoginValidationServiceStub(
					ctx, Utils.getInstance().getProperty("wsdl.login.uri"));
			PortalUserDAO serviceUser = stub.validateUser(userName, password);

			if (serviceUser != null && (IP != null))
				registeredUser = new SystemUser(serviceUser, IP);

			if (registeredUser != null)
				isValid = true;

		} catch (Exception e) {
			// utilLogger.error("Error in accessing the web service", e);
			e.printStackTrace();
		}

		return isValid;
	}

	/**
	 * Obtain the configuration context of the application, in order to encrypt
	 * out going messages and provide WS security.
	 * 
	 * @return configuration context object
	 */
	private ConfigurationContext getConfigurationContext() {

		ConfigurationContext ctx = null;
		try {
			ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client",
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client\\client.axis2.xml");
		} catch (AxisFault e) {

			e.printStackTrace();
		}

		return ctx;
	}

}

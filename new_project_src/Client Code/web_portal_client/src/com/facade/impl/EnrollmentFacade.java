package com.facade.impl;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;

import services.stub.student.Web_portal_StudentActivityServiceStub;
import services.stub.student.Web_portal_StudentActivityServiceStub.ClassStatDAO;
import services.stub.student.Web_portal_StudentActivityServiceStub.EnrollmentDAO;
import services.stub.student.Web_portal_StudentActivityServiceStub.PrerequestCourseDAO;

import com.dao.EnrollmentClientDAO;
import com.dao.GradeStatDAO;
import com.facade.IEnrollmentFacade;
import com.util.Utils;

/*
 *  Description: IEnrollmentFacade implementation
 */

public class EnrollmentFacade implements IEnrollmentFacade {

	/*
	 * Description: used to get the enrolled course
	 */
	@Override
	public EnrollmentClientDAO[] getEnrolledCourses(String loginId) {

		Web_portal_StudentActivityServiceStub stub;
		try {
			ConfigurationContext ctx = getConfigurationContext();
			if (ctx == null)
				return null;

			stub = new Web_portal_StudentActivityServiceStub(ctx, Utils
					.getInstance().getProperty("wsdl.student.activity.uri"));
			EnrollmentDAO[] enrolledServiceCourses = stub
					.getEnrolledCourses(loginId);
			int len = enrolledServiceCourses == null ? 0
					: enrolledServiceCourses.length;

			EnrollmentClientDAO[] enrolledCourses = new EnrollmentClientDAO[len];

			for (int i = 0; i < enrolledServiceCourses.length; i++) {
				enrolledCourses[i] = new EnrollmentClientDAO(
						enrolledServiceCourses[i]);
			}

			return enrolledCourses;

		} catch (Exception e) {
			// utilLogger.error("Error in accessing the web service", e);
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * Description: used to enrolled for the course
	 */
	@Override
	public String[] enrollForCourses(String[] selectedCourses,
			EnrollmentClientDAO[] enrolledCourses, String loginId) {

		String[] msgArr = new String[2];

		if (enrolledCourses != null) {
			for (int i = 0; i < selectedCourses.length; i++) {
				for (int j = 0; j < enrolledCourses.length; j++) {

					if (selectedCourses[i].equals(String
							.valueOf(enrolledCourses[j].getCourseId()))) {
						msgArr[0] = "false";
						msgArr[1] = "You have already enrolled in one or more selected courses. \nPlease select courses that was not taken before.";
						return msgArr;
					}
				}
			}
			if ((selectedCourses.length > (4 - enrolledCourses.length))
					|| (selectedCourses.length > 4)) {
				msgArr[0] = "false";
				msgArr[1] = "You cannot enroll in more than four courses. Sorry for the inconvenience.";
				return msgArr;
			}

			boolean hasPreCourses = this.checkForPreCourses(selectedCourses,
					enrolledCourses);
			if (!hasPreCourses) {
				msgArr[0] = "false";
				msgArr[1] = "There are missing pre-requisite courses for one or more of your selected courses. \nPlease take the pre-requisite courses prior to dependent courses.";
				return msgArr;
			}
		}

		// Calling the web service for data insert.
		try {
			ConfigurationContext ctx = getConfigurationContext();
			if (ctx == null)
				return null;

			Web_portal_StudentActivityServiceStub stub = new Web_portal_StudentActivityServiceStub(
					ctx, Utils.getInstance().getProperty(
							"wsdl.student.activity.uri"));

			boolean isInsertSuccess = stub.insertEnrollment(selectedCourses,
					loginId);
			if (isInsertSuccess) {
				msgArr[0] = "true";
				msgArr[1] = "Enrollment Successful";
				return msgArr;
			}
		} catch (Exception e) {
			// utilLogger.error("Error in accessing the web service", e);
			e.printStackTrace();
		}

		return null;
	}

	/*
	 * Description: used to get enrolled for the course
	 */
	@Override
	public PrerequestCourseDAO[] getPreRequestCourses(int courseId) {

		PrerequestCourseDAO[] preRequestCourse = null;
		try {
			ConfigurationContext ctx = getConfigurationContext();
			if (ctx != null)
				return null;

			Web_portal_StudentActivityServiceStub stub = new Web_portal_StudentActivityServiceStub(
					ctx, Utils.getInstance().getProperty(
							"wsdl.student.activity.uri"));

			preRequestCourse = stub.getPrerequestCourses(courseId);
			return preRequestCourse;

		} catch (Exception e) {
			// utilLogger.error("Error in accessing the web service", e);
			e.printStackTrace();
		}

		return null;
	}

	/*
	 * Description: used to get enrolled for the course
	 */
	private boolean checkForPreCourses(String[] selectedCourses,
			EnrollmentClientDAO[] enrolledCourses) {
		boolean hasAllCourses = false;
		int selectedCourseId = 0;

		for (int i = 0; i < selectedCourses.length; i++) {
			try {
				selectedCourseId = Integer.parseInt(selectedCourses[i]);

				PrerequestCourseDAO[] preRequests = this
						.getPreRequestCourses(selectedCourseId);

				if (preRequests != null) {
					for (int j = 0; j < preRequests.length; j++) {
						boolean hasCourse = false;

						for (int k = 0; k < enrolledCourses.length; k++) {
							if (preRequests[j].getPrereqCourseId() == enrolledCourses[k]
									.getCourseId()) {
								hasCourse = true;
								break;
							}
						}
						if (hasCourse == false) {
							return hasCourse;
						}
					}
				}
				hasAllCourses = true;

			} catch (Exception e) {
				// utilLogger.error("Error in accessing the web service", e);
				e.printStackTrace();
			}
		}
		return hasAllCourses;
	}

	@Override
	public GradeStatDAO[] getClassStats(int courseId, String loginId) {

		GradeStatDAO[] classStatList = null;
		try {
			ConfigurationContext ctx = getConfigurationContext();
			if (ctx != null)
				return null;

			Web_portal_StudentActivityServiceStub stub = new Web_portal_StudentActivityServiceStub(
					ctx, Utils.getInstance().getProperty(
							"wsdl.student.activity.uri"));

			ClassStatDAO[] clsStatsArr = stub.getClassStats(courseId, loginId);
			classStatList = new GradeStatDAO[clsStatsArr.length];

			for (int i = 0; i < clsStatsArr.length; i++) {
				GradeStatDAO gradestatItem = new GradeStatDAO(clsStatsArr[i]);
				classStatList[i] = gradestatItem;
			}
			return classStatList;

		} catch (Exception e) {
			// utilLogger.error("Error in accessing the web service", e);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Obtain the configuration context of the application, in order to encrypt
	 * out going messages and provide WS security.
	 * 
	 * @return configuration context object
	 */
	private ConfigurationContext getConfigurationContext() {

		ConfigurationContext ctx = null;
		try {
			ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client",
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client\\client.axis2.xml");
		} catch (AxisFault e) {

			e.printStackTrace();
		}

		return ctx;
	}

}

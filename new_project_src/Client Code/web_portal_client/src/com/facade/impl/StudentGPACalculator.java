package com.facade.impl;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;

import services.stub.gpa.Web_portal_GPACalculatorServiceStub;

import com.dao.EnrollmentClientDAO;
import com.facade.IStudentGPACalculator;
import com.util.Utils;

/**
 * Class to handle the implementation of student GP values and GPA.
 * 
 */
public class StudentGPACalculator implements IStudentGPACalculator {

	/**
	 * Calculates the gp values for each course, and return it as a Integer
	 * array. This accepts an array of enrolled courses.
	 */
	@Override
	public Integer[] studentGPACalculator(EnrollmentClientDAO enrolClientDAO[]) {

		Integer gpValues[] = null;
		ConfigurationContext ctx = getConfigurationContext();

		if (ctx == null)
			return null;
		try {
			Web_portal_GPACalculatorServiceStub stub = new Web_portal_GPACalculatorServiceStub(
					ctx, Utils.getInstance().getProperty("wsdl.login.uri"));
			if (enrolClientDAO != null) {

				gpValues = new Integer[enrolClientDAO.length];

				for (int i = 0; i < enrolClientDAO.length; i++) {

					if (enrolClientDAO[i].getFinalGrade() != null
							&& (enrolClientDAO[i].getCredit() != null)
							&& (enrolClientDAO[i].getFinalGrade() != "")) {
						int grade = Integer.parseInt(enrolClientDAO[i]
								.getFinalGrade());
						int credit = Integer.parseInt(enrolClientDAO[i]
								.getCredit());
						gpValues[i] = stub.getGP(grade, credit);
					}

				}
			}
		} catch (Exception e) {
			Utils.intLog4J(this.getClass()).info(
					"Exception in connecting to GPA service.");
			e.printStackTrace();
		}

		return gpValues;
	}

	/**
	 * Calculates the GPA for a particular student.
	 * 
	 * @param gpValues
	 *            - an array of GP values for each course
	 * @param enrolClientDAO
	 *            - an array of enrolled courses
	 * @return the GPA value as an Integer.
	 */
	@Override
	public Integer getGPA(Integer[] gpValues,
			EnrollmentClientDAO enrolClientDAO[]) {

		Integer gpa = 0;
		ConfigurationContext ctx = getConfigurationContext();

		if (ctx == null)
			return null;
		try {
			Web_portal_GPACalculatorServiceStub stub = new Web_portal_GPACalculatorServiceStub(
					ctx, Utils.getInstance().getProperty("wsdl.login.uri"));

			if ((enrolClientDAO != null) && (gpValues != null)) {
				int gpSum = 0;
				int creditSum = 0;

				for (int i = 0; i < enrolClientDAO.length; i++) {

					gpSum += gpValues[i];
					creditSum += Integer
							.parseInt(enrolClientDAO[i].getCredit());
				}
				gpa = stub.getGPAverage(creditSum, gpSum);
			}
		} catch (Exception e) {
			Utils.intLog4J(this.getClass()).info(
					"Exception in connecting to GPA service.");
			e.printStackTrace();
		}
		return gpa;
	}

	/**
	 * Obtain the configuration context of the application, in order to encrypt
	 * out going messages and provide WS security.
	 * 
	 * @return configuration context object
	 */
	private ConfigurationContext getConfigurationContext() {

		ConfigurationContext ctx = null;
		try {
			ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client",
							"G:\\SEM 2\\WSLabs\\EclipseWrkSpace_2\\web_portal_client\\client.axis2.xml");
		} catch (AxisFault e) {

			e.printStackTrace();
		}

		return ctx;
	}

}

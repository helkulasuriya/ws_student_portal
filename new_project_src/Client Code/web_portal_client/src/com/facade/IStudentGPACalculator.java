package com.facade;

import com.dao.EnrollmentClientDAO;

/**
 * Interface to access student GP and GPA calculation functions.
 *  
 */
public interface IStudentGPACalculator {

	public Integer[] studentGPACalculator(EnrollmentClientDAO enrolClientDAO[]);

	public Integer getGPA(Integer[] gpValues,
			EnrollmentClientDAO enrolClientDAO[]);
}

package com.facade;

import services.stub.student.Web_portal_StudentActivityServiceStub.PrerequestCourseDAO;

import com.dao.EnrollmentClientDAO;
import com.dao.GradeStatDAO;

/*
 *  Description: Interface  use to describe enrollment methods
 */

public interface IEnrollmentFacade {

	public EnrollmentClientDAO[] getEnrolledCourses(String loginId);

	public String[] enrollForCourses(String[] selectedCourses,
			EnrollmentClientDAO[] enrolledCourses, String loginId);

	public PrerequestCourseDAO[] getPreRequestCourses(int courseId);

	public GradeStatDAO[] getClassStats(int courseId, String loginId);

}

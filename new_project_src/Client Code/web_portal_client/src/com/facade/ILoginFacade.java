package com.facade;

import com.dao.SystemUser;

/*
 *  Description: Interface  use to describe login methods
 */
public interface ILoginFacade {

	public boolean validateUser(String userName, String password, String IP);

	public SystemUser getRegisteredUser();

	public boolean changePassword(String loginId, String oldPassword,
			String newPassword);

	enum USER_TYPE {
		STUDENT, TEACHER
	}
}

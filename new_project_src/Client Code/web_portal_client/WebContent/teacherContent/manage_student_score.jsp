<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="javax.naming.Context"%>
<%@ page import="javax.naming.InitialContext"%>
<%@ page import="javax.naming.NamingException"%>
<%@ page import="javax.sql.DataSource"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.ResultSet"%>


<c:set var="teacherId" value="1" />
<c:set var="courseId" value='${param.course_id}' />

<sql:query var="rs" dataSource="jdbc/student_portal">
	SELECT co.title,cp.practice_id,cp.practice_type, cp.date, cp.description, cp.last_modified, cp.percentage FROM course_practice cp,course co where cp.course_id=co.course_id and cp.course_id=?
	<sql:param value="${courseId}" />
</sql:query>

<%@ include file="../template/header.jsp"%>

<c:forEach var="row" items="${rs.rows}">
	<c:set var="title" value="${row.title}" scope="page" />
</c:forEach>

<div id="content">

	<div class="section_break">
		<div id="content">
			<div id="title_tbl">${title}</div>
			<table class="gridtable">
				<tr>
					<th>Practice ID</th>
					<th>Practice Type</th>
					<th>Scheduled on</th>
					<th>Description</th>
					<th>Percentage</th>
					<th>Action</th>

				</tr>
				<c:forEach var="row" items="${rs.rows}">
					<tr>

						<td style="text-align: center;">${row.practice_id}</td>

						<td>${row.practice_type}</td>

						<c:choose>
							<c:when
								test="${fn:toLowerCase(row.practice_type) == fn:toLowerCase('FINAL_EXAM')}">

								<td><input type="text" name="final_exam_dt"
									id="final_exam_dt" readonly="true" value="" /></td>
								<input type="hidden" name="final_exam_dt_hidden"
									id="final_exam_dt_hidden" size="1" readonly="true"
									value="${row.date}" />
								<input type="hidden" name="final_exam_dt_id"
									id="final_exam_dt_id" size="1" readonly="true"
									value="${row.practice_id}" />

								<script type="text/javascript">
									eval("$('#final_exam_dt').datetimepicker({ altFormat: 'yy-mm-dd' });");
									$('#final_exam_dt').datetimepicker(
											'setDate',
											(new Date(
													$('#final_exam_dt_hidden')
															.val())));
								</script>

								<td>${row.description}</td>
								<td><input type="text" name="final_exam_per"
									id="final_exam_per" size="4" value="${row.percentage}" /></td>
								<td style="text-align: center;"><input
									id="update_row${row.practice_id}" class="button_text"
									onclick='updateDate(0)' type="button" name="submit"
									value="Update" /></td>

							</c:when>
							<c:when
								test="${fn:toLowerCase(row.practice_type) == fn:toLowerCase('ASSIGNMENT')}">

								<td><input type="text" name="assignment_dt"
									id="assignment_dt" readonly="true" value="" /></td>
								<input type="hidden" name="assignment_dt_hidden"
									id="assignment_dt_hidden" size="1" readonly="true"
									value="${row.date}" />
								<input type="hidden" name="assignment_dt_id"
									id="assignment_dt_id" size="1" readonly="true"
									value="${row.practice_id}" />

								<script type="text/javascript">
									eval("$('#assignment_dt').datetimepicker()");
									$('#assignment_dt').datetimepicker(
											'setDate',
											(new Date(
													$('#assignment_dt_hidden')
															.val())));
								</script>

								<td>${row.description}</td>
								<td><input type="text" name="assignment_per"
									id="assignment_per" size="4" value="${row.percentage}" /></td>
								<td style="text-align: center;"><input
									id="update_row${row.practice_id}" class="button_text"
									onclick='updateDate(1)' type="button" name="submit"
									value="Update" /></td>

							</c:when>
							<c:when
								test="${fn:toLowerCase(row.practice_type) == fn:toLowerCase('QUIZZES')}">

								<td><input type="text" name="quezz_dt" id="quezz_dt"
									readonly="true" value="" /></td>
								<input type="hidden" name="quezz_dt_hidden" id="quezz_dt_hidden"
									size="1" readonly="true" value="${row.date}" />
								<input type="hidden" name="quezz_dt_id" id="quezz_dt_id"
									size="1" readonly="true" value="${row.practice_id}" />

								<script type="text/javascript">
									eval("$('#quezz_dt').datetimepicker()");
									$('#quezz_dt').datetimepicker(
											'setDate',
											(new Date($('#quezz_dt_hidden')
													.val())));
								</script>

								<td>${row.description}</td>
								<td><input type="text" name="quezz_per" id="quezz_per"
									size="4" value="${row.percentage}" /></td>

								<td style="text-align: center;"><input
									id="update_row${row.practice_id}" class="button_text"
									onclick='updateDate(2)' type="button" name="submit"
									value="Update" /></td>
							</c:when>
							<c:when
								test="${fn:toLowerCase(row.practice_type) == fn:toLowerCase('MIDTEARM')}">

								<td><input type="text" name="midterm_dt" id="midterm_dt"
									readonly="true" value="" /></td>
								<input type="hidden" name="midterm_dt_hidden"
									id="midterm_dt_hidden" size="1" readonly="true"
									value="${row.date}" />
								<input type="hidden" name="midterm_dt_id" id="midterm_dt_id"
									size="1" readonly="true" value="${row.practice_id}" />

								<script type="text/javascript">
									eval("$('#midterm_dt').datetimepicker()");
									$('#midterm_dt').datetimepicker(
											'setDate',
											(new Date($('#midterm_dt_hidden')
													.val())));
								</script>


								<td>${row.description}</td>
								<td><input type="text" name="midterm_per" id="midterm_per"
									size="4" value="${row.percentage}" /></td>

								<td style="text-align: center;"><input
									id="update_row${row.practice_id}" class="button_text"
									onclick='updateDate(3)' type="button" name="submit"
									value="Update" /></td>
							</c:when>
							<c:otherwise>

							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
			</table>



		</div>

		<script type="text/javascript">
			
		</script>
	</div>


</div>


<%@ include file="../template/footer.html"%>



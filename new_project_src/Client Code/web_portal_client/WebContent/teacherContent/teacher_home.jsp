<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="javax.naming.Context"%>
<%@ page import="javax.naming.InitialContext"%>
<%@ page import="javax.naming.NamingException"%>
<%@ page import="javax.sql.DataSource"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="com.dao.SystemUser"%>

<%@ page session="true"%>

<c:set var="teacherId" value="1" />

<sql:query var="rs" dataSource="jdbc/student_portal">
	SELECT c.course_id, c.title, c.time, c.term, assigned_on
	FROM course c, course_teaching ct WHERE 1 =1 
	AND registered_user_id =? AND c.course_id=ct.course_id 
	ORDER BY time ASC LIMIT 0 , 30
	<sql:param value="${teacherId}" />
</sql:query>


<%@ include file="../template/header.jsp"%>

<div id="welcomeMsg">${sessionScope.user}</div>
<br />
<div class="menue_div_bottom">

	<div id="content">
		<table class="gridtable">
			<tr>
				<th>Actions</th>
				<th>Course ID</th>
				<th>Course Name</th>
				<th>Time</th>
				<th>Term</th>
				<th>Assigned on</th>
			</tr>

			<c:forEach var="row" items="${rs.rows}">
				<tr>

					<td style="text-align: center;"><a
						href="manage_course_content.jsp?course_id=${row.course_id}"
						class="tooltip"> <img src="../img/view.gif" /> <span>
								<img class="callout" src="../img/callout.gif" /> <strong>Modify
									course content..</strong> <br /> <img src="../img/tooltip.JPG"
								style="float: right;" /> Modify course details including
								syllabus, assignments, notes and etc.
						</span>
					</a> <a href="manage_course_dates.jsp?course_id=${row.course_id}"
						class="tooltip"> <img src="../img/update.png" /> <span>
								<img class="callout" src="../img/callout.gif" /> <strong>Update
									course practice dates..</strong> <br /> <img src="../img/tooltip.JPG"
								style="float: right;" /> Modify course dates for assignments,
								quizzes, midterm(s), final exam
						</span>
					</a> <a href="manage_student_enrollment.jsp?course_id=${row.course_id}"
						class="tooltip"> <img src="../img/mange.gif" /> <span>
								<img class="callout" src="../img/callout.gif" /> <strong>Manage
									Enrollemnts ..</strong> <br /> <img src="../img/tooltip.JPG"
								style="float: right;" /> Manage student score based on
								assignments, quiz and exams.
						</span>
					</a> <a href="#" class="tooltip"> <img src="../img/statistics.png" />
							<span> <img class="callout" src="../img/callout.gif" /> <strong>Course
									statistics..</strong> <br /> <img src="../img/tooltip.JPG"
								style="float: right;" /> View student GPA, and update the final
								marks.
						</span>
					</a></td>

					<td>${row.course_id}</td>
					<td>${row.title}</td>
					<td>${row.time}</td>
					<td>${row.term}</td>
					<td>${row.assigned_on}</td>
				</tr>
			</c:forEach>
		</table>
	</div>


	<%@ include file="../template/footer.html"%>
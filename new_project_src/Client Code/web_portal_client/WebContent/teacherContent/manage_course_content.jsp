<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="javax.naming.Context"%>
<%@ page import="javax.naming.InitialContext"%>
<%@ page import="javax.naming.NamingException"%>
<%@ page import="javax.sql.DataSource"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.ResultSet"%>

<c:set var="teacherId" value="1" />
<c:set var="courseId" value='${param.course_id}' />

<sql:query var="rs" dataSource="jdbc/student_portal">
	select co.course_id, co.title, 	co.location,co.credit,	co.time,	co.term,attrib.attribute_id, attrib.attribute_type, attrib.description,users.name ,attrib.last_modified 
	from student_portal.course_attributes attrib, registered_user users , course co
	WHERE 1 =1 
	AND co.course_id=attrib.course_id
	AND attrib.course_id=? 
	AND users.registered_user_id=?
	<sql:param value="${courseId}" />
	<sql:param value="${teacherId}" />
</sql:query>


<c:forEach var="row" items="${rs.rows}">
	<c:set var="course_id" value="${row.course_id}" scope="page" />
	<c:set var="name" value="${row.title}" scope="page" />
	<c:set var="location" value="${row.location}" scope="page" />
	<c:set var="credit" value="${row.credit}" scope="page" />
	<c:set var="time" value="${row.time}" scope="page" />
	<c:set var="term" value="${row.term}" scope="page" />
</c:forEach>

<script type="text/javascript">
	//$('#quezz_dt').datetimepicker('setDate', (new Date('05/31/2013 15:54')) );
</script>

<%@ include file="../template/header.jsp"%>

<div id="content">

	<img id="top" src="../img/top.png" alt="" />
	<div id="form_container">

		<form id="course_form" class="appnitro" method="post"
			action="../CourseContentManager">
			<div class="form_description">
				<h2>Course : ${name} [${course_id}]</h2>
				<p>Below fields display the course information.. Click Update to
					save the modifications.</p>

			</div>
			<div id='error'></div>
			<ul>

				<li id="li_1"><label class="description" for="course_id">Course
						Reg. No </label>
					<div>

						<input id="course_id" name="course_id" class="element text small"
							type="text" readonly=ture maxlength="255" value="${course_id}" />
					</div></li>
				<li id="li_3"><label class="description" for="course_title">Course
						Title </label>
					<div>
						<input id="course_title" name="course_title"
							class="element text large" type="text" maxlength="255"
							value="${name}" />
					</div></li>
				<li id="li_2">


					<table border="0" bordercolor="#FFCC00" style=""
						100%" cellpadding="0" cellspacing="3">
						<tr>
							<td><label class="description" for="course_location">Location
							</label></td>
							<td><label class="description" for="course_term">&nbsp;&nbsp;&nbsp;
							</label></td>
							<td><label class="description" for="course_term">Term
							</label></td>
						</tr>
						<tr>
							<td><div>
									<input id="course_location" name="course_location"
										style="width: 100%" class="element text small" type="text"
										maxlength="255" value="${location}" />
								</div></td>
							<td>&nbsp;&nbsp;&nbsp;</td>
							<td><div>
									<select class="element select small" id="course_term"
										style="width: 100%" name="course_term">

										<c:choose>


											<c:when
												test="${fn:toLowerCase(term) == fn:toLowerCase('Term 1 (Jan-Apr)')}">

												<option value="To be confirmed">To be confirmed</option>
												<option value="Term 1 (Jan-Apr)" selected="selected">Term
													1 (Jan-Apr)</option>
												<option value="Term 2 (Apr-Jul)">Term 2 (Apr-Jul)</option>
											</c:when>

											<c:when
												test="${fn:toLowerCase(term) == fn:toLowerCase('Term 2 (Apr-Jul)')}">

												<option value="To be confirmed">To be confirmed</option>
												<option value="Term 1 (Jan-Apr)">Term 1 (Jan-Apr)</option>
												<option value="Term 2 (Apr-Jul)" selected="selected">Term
													2 (Apr-Jul)</option>
											</c:when>
											<c:otherwise>
												<option value="To be confirmed" selected="selected">To
													be confirmed</option>
												<option value="Term 1 (Jan-Apr)">Term 1 (Jan-Apr)</option>
												<option value="Term 2 (Apr-Jul)">Term 2 (Apr-Jul)</option>
											</c:otherwise>
										</c:choose>

									</select>
								</div></td>
						</tr>
					</table>

				</li>
				<li id="li_7"></li>

				<li id="li_4"><label class="description" for="course_credits">Credit
						points </label>
					<div>
						<input id="course_credits" name="course_credits"
							class="element text small" type="text" maxlength="255"
							value="${credit}" />
					</div></li>
				<li id="li_5"><label class="description" for="element_5">Lecture
						Time </label> <span> <input id="course_time" name="course_time"
						class="element text " size="12" type="text" maxlength="12"
						value="${time}" /> : <label>HH:MM</label>
				</span></li>

				<li class="buttons"><input id="saveForm" class="button_text"
					type="submit" name="submit" value="Update" /></li>

			</ul>
		</form>

	</div>
	<img id="bottom" src="../img/bottom.png" alt="" />

	<div class="section_break">
		<div id="content">
			<div id="title_tbl">Test</div>
			<table class="gridtable">
				<tr>
					<th>Actions</th>
					<th>Attribute ID</th>
					<th>Attribute Type</th>
					<th>Description</th>
					<th>Last modified</th>

				</tr>
				<c:forEach var="row" items="${rs.rows}">


					<tr>

						<td style="text-align: center;"><a
							href="../plugins/jwysiwyg/editor.html?view=true&id= ${row.attribute_id} &user=${teacherId}&type=${row.attribute_type}"
							title="" class="tooltip" name="windowX"> <img
								src="../img/view.gif" /> <span> <img class="callout"
									src="../img/callout.gif" /> <strong>View course
										content..</strong> <br /> <img src="../img/tooltip.JPG"
									style="float: right;" /> View course details including
									syllabus, assignments, notes and etc.
							</span>
						</a> <a
							href="../plugins/jwysiwyg/editor.html?view=false&id= ${row.attribute_id} &user=${teacherId}&type=${row.attribute_type}"
							title="" class="tooltip" name="windowX"> <img
								src="../img/update.png" /> <span> <img class="callout"
									src="../img/callout.gif" /> <strong>Update course
										content..</strong> <br /> <img src="../img/tooltip.JPG"
									style="float: right;" /> Modify course details including
									syllabus, assignments, notes and etc.
							</span>
						</a> <script type="text/javascript">
							$('.tooltip').popupWindow({
								centerBrowser : 1,
								width : 850,
								height : 330
							});
						</script></td>

						<td>${row.attribute_id}</td>
						<td>${row.attribute_type}</td>
						<td>${row.description}</td>
						<td>${row.name} ::: ${row.last_modified}</td>
					</tr>
				</c:forEach>
			</table>

		</div>
	</div>

</div>


<%@ include file="../template/footer.html"%>



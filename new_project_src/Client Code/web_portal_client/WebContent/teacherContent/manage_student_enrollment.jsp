<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="javax.naming.Context"%>
<%@ page import="javax.naming.InitialContext"%>
<%@ page import="javax.naming.NamingException"%>
<%@ page import="javax.sql.DataSource"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.ResultSet"%>


<c:set var="teacherId" value="1" />
<c:set var="courseId" value='${param.course_id}' />

<sql:query var="rs" dataSource="jdbc/student_portal">
	select ce.registered_user_id,co.title,co.credit, usr.name, ce.final_grade, ce.enrolled_time from `student_portal`.`course_enrollment` ce,
	`student_portal`.`registered_user` usr, `student_portal`.`course` co
	where  1=1 and 
	ce.registered_user_id=usr.registered_user_id and
	co.course_id=ce.course_id
	and ce.course_id=?
	<sql:param value="${courseId}" />
</sql:query>



<%@ include file="../template/header.jsp"%>

<c:forEach var="row" items="${rs.rows}">
	<c:set var="title" value="${row.title}" scope="page" />
</c:forEach>

<div id="content">

	<div class="section_break">
		<div id="content">
			<div id="title_tbl">Studetnt enrollemnts : ${title}</div>
			<table class="gridtable">
				<tr>
					<th>Actions</th>
					<th>Studnet Name</th>
					<th>Final Grade</th>
					<th>Enrolled Time</th>
				</tr>
				<c:forEach var="row" items="${rs.rows}">


					<tr>
						<td style="text-align: center;"><a
							href="manage_student_score.jsp?student_id=${row.registered_user_id}&course_id=${param.course_id}"
							class="tooltip"> <img src="../img/update.png" /> <span>
									<img class="callout" src="../img/callout.gif" /> <strong>View
										/ Update score..</strong> <br /> <img src="../img/tooltip.JPG"
									style="float: right;" /> Modify marks for assignments,
									quizzes, midterm(s), final exam
							</span>
						</a> <a href="#" class="tooltip"> <img src="../img/mange.gif" />
								<span> <img class="callout" src="../img/callout.gif" />
									<strong>Manage GPA..</strong> <br /> <img
									src="../img/tooltip.JPG" style="float: right;" /> Manage
									student GPA on assignments, quiz and exams.
							</span></td>
						<td>${row.name}</td>
						<td>${row.final_grade}</td>
						<td>${row.enrolled_time}</td>
					</tr>
				</c:forEach>
			</table>
		</div>

		<script type="text/javascript">
			
		</script>
	</div>
</div>


<%@ include file="../template/footer.html"%>



<%@page import="com.dao.SystemUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<title>My Site</title>
	
	<link media="all" type="text/css" href="css/core.css" rel="stylesheet"></link>	
    <link media="all" type="text/css" href="../css/view.css" rel="stylesheet"></link>
	<link media="all" type="text/css" href="css/validationEngine.jquery.css" rel="stylesheet" />

	<script src="js/jquery-1.8.3.min.js" type="text/javascript"></script>
	<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
	<script src="../js/jquery-ui.js"></script>  
	<script type="text/javascript">
		$(document).ready(function (){
			
			jQuery("#validateSuccess").validationEngine();
		   
		});
		
/* $("#validateSuccess").on('submit',function(e){



$('#loginusername_erro').show();
$('#loginpassword_erro').show();
$('#login_erro').show();

// validate
// make the REST call
// keep a count
// submit the request to the intiateSession.jsp page.

alert('validate please');
e.preventDefault();
}); */
	
	</script>
</head>

<body>
<% 
	Integer loginAttempts = (Integer) session.getAttribute("loginAttempts");

	SystemUser loggedInUser = (SystemUser) session.getAttribute("loggedInUser");
	if(loggedInUser != null){
		response.sendRedirect("studentContent/student_home.jsp");
		return;
	}
%>
<div id="wrap">
<form id="validateSuccess" action="LoginServlet" method="post">
 <table align="left" width="100%" border="1" cellpadding="10" cellspacing="5">
               <tbody><tr>
                  <td>                  	 
                     <input name="authenticate" value="true" type="hidden" />
                     <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
						<tr>
                          <td style="font-size:18px" align="left" nowrap="nowrap">
                            <span>
                            <img id="avc_logo_image" src="img/title.png" style=" padding-bottom: 10px; border:0px" />
                            </span>
                          </td>

                       </tr>
                       <!-- Checking for login attempts -->
                       <c:if test="${(loginAttempts != null)}">
                       		<c:choose>
                       			<c:when test="${(loginAttempts > 0) && (loginAttempts <= 3)}">                       			
	                       			<tr>
						                <td colspan="2">
						                	<label style="color:red"><i>Invalid username or password. Please try again..</i></label>	                       			                       		
						                </td>
				                    </tr>
                       			</c:when>
                       			<c:when test="${(loginAttempts > 3)}">
                       				<tr>
						                <td colspan="2">
						                	<label style="color:red"><i>We are sorry. Maximum login attempts exceeded.</i></label>	                       			                       		
						                </td>
				                    </tr>
                       			</c:when>
                       		</c:choose>
                       	</c:if>                                            
                       <tr>
                          <td colspan="2">
                          	 <br />                         	 
                             <label class="" id="username" for="username">Username:</label>
                          </td>
                       </tr>
                       <tr>
                           <td colspan="2">
                              <input id="loginusername" style="width: 100%;" class="validate[required,maxSize[30]]" size="30" name="username" value="" type="text" />

                           </td>
                        </tr>
                        <tr>
                           <td colspan="2" style="padding-top:10px">
                              <label class="" id="password" for="password">Password:</label>
							  

                           </td>
                        </tr>

                        <tr>
                           <td colspan="2">
                              <input id="loginpassword" style="width: 100%;" class="validate[required,minSize[6],maxSize[12]]" size="30" name="password" value="" type="password" />

                           </td>
                        </tr>

                        <tr>
                           <td colspan="2" style="padding-top:10px" align="right">
                           		<a href="">Forgot your password?</a>
                           		
                           		<!-- Disabling login button if maximum login attempts exceeded. -->                           		
                           		<c:choose>
                           			<c:when test="${loginAttempts > 3}">                           				
                           				<span class=" avc_login_submit_end">&nbsp;</span><input class="" id="login" value="Login >" type="submit" disabled="disabled" /><span class=" avc_login_submit_end">&nbsp;</span>                         				
                           			</c:when>
                           			<c:otherwise>
                           				<span class=" avc_login_submit_end">&nbsp;</span><input class="" id="login" value="Login >" type="submit" /><span class=" avc_login_submit_end">&nbsp;</span>                           				
                           			</c:otherwise>
                           		</c:choose>                           		                              							  
							  <br/>
                           </td>                     
                        </tr>
                        
                     </tbody>
                     </table>
                  </td>
               </tr>
            </tbody>
	       </table>
		</form>
			</div>

</body>
</html>
<%@page import="com.facade.impl.StudentGPACalculator"%>
<%@page import="com.facade.IStudentGPACalculator"%>
<%@page import="com.dao.EnrollmentClientDAO"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="../template/header.jsp"%>

<%
	Integer[] gpValues = null;
	Integer gpa = 0;
	EnrollmentClientDAO[] enrolledCoursesArr = null;

	enrolledCoursesArr = (EnrollmentClientDAO[]) session
			.getAttribute("enrolledCourses");

	IStudentGPACalculator gpaCalculator = new StudentGPACalculator();
	// Obtain the GP values for each course
	gpValues = gpaCalculator.studentGPACalculator(enrolledCoursesArr);
	// Obtain the GPA.
	gpa = gpaCalculator.getGPA(gpValues, enrolledCoursesArr);
%>
<c:set var="enrolledCourses" value="${enrolledCoursesArr}"></c:set>

<div id="content">
	<form action="../MainServlet?request=calcGPA" method="post">
		<table class="gridtable">
			<tr>
				<th>Course Name</th>
				<th>Course GP</th>
			</tr>
			<%
				int iterate = 0;
			%>
			<c:if test="${enrolledCourses != null}">
				<c:forEach var="course" items="${enrolledCourses}">

					<tr>
						<td>${course.courseName}</td>

						<td><%= gpValues[iterate] %></td>
					</tr>
					<%
						iterate += 1;
					%>
				</c:forEach>
			</c:if>
			<tr>
				<td>GPA Value :</td>

				<td><input type="text" id="gpaVal" name="gpaVal"
					disabled="disabled" value="<%=gpa%>" /> <input type="text"
					id="submitted" name="submitted" hidden="hidden" value="submitted" /></td>
			</tr>
			<tr>
				<td><input id="resetBtn" class="button_text" type="reset"
					name="reset" value="Reset" /></td>

				<td><input id="get_course_gp" class="button_text" type="submit"
					name="submit" value="Get Course GP" disabled="disabled" /></td>
			</tr>
		</table>
	</form>
</div>

<%@ include file="../template/footer.html"%>
<%@page import="com.dao.SystemUser"%>
<%@page import="com.dao.EnrollmentClientDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="../template/header.jsp"%>

<%
	EnrollmentClientDAO[] enrolledCoursesArr = (EnrollmentClientDAO[]) session
			.getAttribute("enrolledCourses");
	SystemUser loggedInUser = (SystemUser) session
			.getAttribute("loggedInUser");
%>
<c:set var="enrolledCourses" value="<%=enrolledCoursesArr%>" />
<c:set var="loginId" value="<%=loggedInUser.getUserName()%>" />

<div id="content">

	<table class="gridtable">
		<tr>
			<th>Course Name</th>
			<th>Practice Name</th>
			<th>Practice Type</th>
			<th>Due Date</th>
			<th>Description</th>
		</tr>
		<c:if test="${enrolledCourses != null}">

			<c:forEach var="course" items="${enrolledCourses}">

				<sql:query var="practices" dataSource="jdbc/student_portal">
					select cp.practice_id, cp.course_id, c.name as courseName, cp.name as practiceName, cp.practice_type, cp.date, cp.description 
					from student_portal.course_practice cp
					inner join student_portal.course c on cp.course_id = c.course_id 
					where cp.created_date > (select last_login_time from student_portal.user_login where login_id=?) 
					and cp.course_id=?;
				<sql:param value="${loginId}" />
					<sql:param value="${course.courseId}" />
				</sql:query>

				<c:forEach var="updatedPractice" items="${practices}">
					<tr>
						<td>${updatedPractice.courseName}</td>
						<td>${updatedPractice.practiceName}</td>
						<td>${updatedPractice.practice_type}</td>
						<td>${updatedPractice.date}</td>
						<td>${updatedPractice.description}</td>
					</tr>
				</c:forEach>
			</c:forEach>

		</c:if>
	</table>
</div>

<%@ include file="../template/footer.html"%>
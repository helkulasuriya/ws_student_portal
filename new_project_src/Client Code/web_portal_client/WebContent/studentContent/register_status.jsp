<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ include file="../template/header.jsp"%>

<div id="content">

	<% 
		String isSuccess = request.getParameter("isSuccess");
		String message = request.getParameter("message");	
	%>

	<p>
		<c:out value="${message}" />
	</p>
</div>

<%@ include file="../template/footer.html"%>
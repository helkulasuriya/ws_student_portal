<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<%@ include file="../template/header.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		$('#element_2').change(function() {

			var value = $("#element_2 option:selected").val();

			if (value == "idRange") {
				$('#selectExample').text("  e.g.: Insert range as 1-4");
			} else {
				$('#selectExample').text("");
			}
		});
	});
</script>

<div id="content">

	<img id="top" src="../img/top.png" alt="" />
	<div id="form_container">

		<form id="course_form" class="appnitro" method="post"
			action="../MainServlet?request=search">
			<div class="form_description">
				<h3>Insert Search Criteria :</h3>
			</div>

			<div>
				<input id="element_1" name="searchTxt" class="element text small"
					type="text" maxlength="255" value="" /> <label id="selectExample"
					for="searchCriteria"></label>
			</div>
			<br />
			<div>
				<select class="element select small" id="element_2" style=""
					name="searchCriteria">
					<option value="name" selected="selected">Course Name</option>
					<option value="term">Term</option>
					<option value="courseId">Course Id</option>
					<option value="idRange">Course Id Range</option>
				</select>
			</div>
			<br />
			<div>
				<input id="searchBtn" class="button_text" type="submit"
					name="searchBtn" value="Search" />
			</div>
		</form>

	</div>
	<img id="bottom" src="../img/bottom.png" alt="" />

</div>

<%@ include file="../template/footer.html"%>
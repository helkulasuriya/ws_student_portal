<%@page import="com.dao.SystemUser"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="javax.naming.Context"%>
<%@ page import="javax.naming.InitialContext"%>
<%@ page import="javax.naming.NamingException"%>
<%@ page import="javax.sql.DataSource"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.ResultSet"%>

<c:set var="courseId" value="<%= request.getParameter(\"course_id\") %>" />
<%
	SystemUser user = (SystemUser) session.getAttribute("loggedInUser");
%>
<c:set var="userId" value="<%=  user.getUserName() %>" />

<sql:query var="courses" dataSource="jdbc/student_portal">	
	select * from student_portal.course where course_id = ?
	
	<sql:param value="${courseId}" />
</sql:query>

<sql:query var="rs" dataSource="jdbc/student_portal">	
	select cp.name, cp.practice_type, cp.date, cp.description, pa.grade 
	from student_portal.course c 
	inner join student_portal.course_practice cp on c.course_id = cp.course_id 
	inner join student_portal.course_participation pa on pa.practice_id = cp.practice_id
	where c.course_id=? 
	and pa.registered_user_id = (select registered_user_id from student_portal.user_login where login_id=?)
	
	<sql:param value="${courseId}" />
	<sql:param value="${userId}" />
</sql:query>



<%@ include file="../template/header.jsp"%>





<div id="content">

	<img id="top" src="../img/top.png" alt="" />
	<div id="form_container">

		<form id="course_form" class="appnitro" method="post" action="">
			<div class="form_description">
				<h2>Course :</h2>
				<p>Course Description</p>
			</div>
			<ul>
				<c:forEach var="course" items="${courses.rows}">
					<li id="li_1"><label class="description" for="element_1">Course
							Reg. No </label>
						<div>
							<input id="element_1" name="element_1" class="element text small"
								type="text" maxlength="255" value="${course.course_id}"
								disabled="disabled" />
						</div></li>
					<li id="li_3"><label class="description" for="element_3">Course
							Title </label>
						<div>
							<input id="element_3" name="element_3" class="element text large"
								type="text" maxlength="255" value="${course.name}"
								disabled="disabled" />
						</div></li>
					<li id="li_2">


						<table border="0" bordercolor="#FFCC00" style=""
							100%" cellpadding="0" cellspacing="3">
							<tr>
								<td><label class="description" for="element_2">Location
								</label></td>
								<td><label class="description" for="element_7">&nbsp;&nbsp;&nbsp;
								</label></td>
								<td>
									<!-- <label class="description" for="element_7">Term </label>  -->
								</td>
							</tr>
							<tr>
								<td><div>
										<input id="element_2" name="element_2" style="width: 100%"
											class="element text small" type="text" maxlength="255"
											value="${course.location}" disabled="disabled" />
									</div></td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td>
									<!-- <div>
		        <select class="element select small" id="element_7" style="width:100%" name="element_7"> 
		            <option value="0" selected="selected">To be confirmed</option>
		<option value="1" >Term 1 (Jan-Apr)</option>
		<option value="2" >Term 2 (Apr-Jul)</option>
		<option value="3" >Term 3 (Jul-Sep)</option>
		<option value="3" >Term 4 (Sep-Dec)</option>
		
		        </select>
		        </div>  -->
								</td>
							</tr>
						</table>

					</li>
					<li id="li_7"></li>

					<li id="li_4"><label class="description" for="element_4">Credit
							thrshold </label>
						<div>
							<input id="element_4" name="element_4" class="element text small"
								type="text" maxlength="255" value="${course.credit}"
								disabled="disabled" />
						</div></li>
					<li id="li_5"><label class="description" for="element_5">Lecture
							Time </label>
						<div>
							<input id="element_5" name="element_5" class="element text small"
								type="text" maxlength="255" value="${course.time}"
								disabled="disabled" />
						</div></li>
				</c:forEach>

			</ul>
		</form>

	</div>
	<img id="bottom" src="../img/bottom.png" alt="" />

	<div class="section_break">
		<div id="content">
			<div id="title_tbl"></div>
			<table class="gridtable">
				<tr>
					<th>Practice Name</th>
					<th>Practice Type</th>
					<th>Due Date</th>
					<th>Description</th>
					<th>Grade</th>

				</tr>

				<c:forEach var="practiceRow" items="${rs.rows}">
					<tr>
						<td>${practiceRow.name}</td>
						<td>${practiceRow.practice_type}</td>
						<td>${practiceRow.date}</td>
						<td>${practiceRow.description}</td>
						<td>${practiceRow.grade}</td>
					</tr>
				</c:forEach>
			</table>

		</div>
		<div id="title_tbl">
			<a href="class_stats.jsp?courseId=${courseId}">View Class
				Statistics</a>
		</div>
	</div>


</div>


<%@ include file="../template/footer.html"%>



<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="../template/header.jsp"%>

<%
	String courseId1 = request.getParameter("id");
	String courseName1 = request.getParameter("cname");
%>
<c:set var="courseName" value="${courseName1}">
<c:set var="courseId" value="${courseId1}">

<sql:query var="attributes" dataSource="jdbc/student_portal">
	SELECT attribute_id, course_id, attribute_type, description, value 
	FROM student_portal.course_attributes 
	WHERE course_id=?
	<sql:param value="${courseId}" />
</sql:query>


	<div id="content">

		<table class="gridtable">
			<tr>
				<td><strong>Course Name:</strong></td>
				<td><c:out value="${courseName}" /></td>
			</tr>
			<c:forEach var="attribute" items="${attributes.rows}">
				<tr>
					<td>${attribute.attribute_type}</td>
					<td>${attribute.value}</td>
				</tr>
			</c:forEach>
		</table>
	</div>

	<%@ include file="../template/footer.html"%>
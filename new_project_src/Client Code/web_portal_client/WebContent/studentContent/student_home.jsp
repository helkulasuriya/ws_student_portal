
<%@page import="com.dao.SystemUser"%>
<%@page import="com.dao.EnrollmentClientDAO"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="../template/header.jsp"%>

<%
	EnrollmentClientDAO[] enrolledCourses = (EnrollmentClientDAO[]) session
			.getAttribute("enrolledCourses");
%>

<div id="content">
	<div id="title_tbl">Enrolled Courses</div>
	<table class="gridtable">
		<tr>
			<th>Actions</th>
			<th>Course ID</th>
			<th>Course Name</th>
			<th>Time</th>
			<th>Final Grade</th>

		</tr>

		<c:forEach var="course" items="${enrolledCourses}">
			<c:if test="${course.courseName != null}">
				<tr>
					<td style="text-align: center;"><a
						href="course_info_form.jsp?course_id=${course.courseId}"
						class="tooltip"> <img src="../img/Statistics.gif" /> <span>
								<img class="callout" src="../img/callout.gif" /> <strong>View
									course content..</strong> <br /> <img src="../img/tooltip.JPG"
								style="float: right;" /> View current course information
								including course date, time and, attribute grades related
								updates.
						</span>
					</a> <a
						href="course_detail.jsp?id=${course.courseId}&cname=${course.courseName}"
						class="tooltip"> <img src="../img/view.gif" /> <span>
								<img class="callout" src="../img/callout.gif" /> <strong>View
									course content..</strong> <br /> <img src="../img/tooltip.JPG"
								style="float: right;" /> View course details including
								syllabus, assignments, notes and etc.
						</span>
					</a></td>

					<td>${course.courseId}</td>
					<td>${course.courseName}</td>
					<td>${course.courseTime}</td>
					<td>${course.finalGrade}</td>
				</tr>
			</c:if>
		</c:forEach>
	</table>

	<br /> <br />

	<div>
		<%
			EnrollmentClientDAO[] enrolledCoursesArr = (EnrollmentClientDAO[]) session
					.getAttribute("enrolledCourses");
			SystemUser loggedInUser = (SystemUser) session
					.getAttribute("loggedInUser");
		%>
		<c:set var="enrolledCourses" value="<%=enrolledCoursesArr%>" />
		<c:set var="loginId" value="<%=loggedInUser.getUserName()%>" />

		<div id="title_tbl">Announcements</div>
		<table class="gridtable">
			<tr>
				<th>Course Name</th>
				<th>Practice Name</th>
				<th>Practice Type</th>
				<th>Due Date</th>
				<th>Description</th>
			</tr>
			<c:if test="${enrolledCourses != null}">

				<c:forEach var="course" items="${enrolledCourses}">

					<sql:query var="practices" dataSource="jdbc/student_portal">
						select cp.practice_id, cp.course_id, c.name as courseName, cp.name as practiceName, cp.practice_type, cp.date, cp.description 
						from student_portal.course_practice cp
						inner join student_portal.course c on cp.course_id = c.course_id 
						where cp.created_date > (select last_login_time from student_portal.user_login where login_id=?) 
						and cp.course_id=?;
						<sql:param value="${loginId}" />
						<sql:param value="${course.courseId}" />
					</sql:query>

					<c:forEach var="updatedPractice" items="${practices.rows}">
						<tr>
							<td>${updatedPractice.courseName}</td>
							<td>${updatedPractice.practiceName}</td>
							<td>${updatedPractice.practice_type}</td>
							<td>${updatedPractice.date}</td>
							<td>${updatedPractice.description}</td>
						</tr>
					</c:forEach>
				</c:forEach>
			</c:if>
		</table>
	</div>
</div>

<%@ include file="../template/footer.html"%>
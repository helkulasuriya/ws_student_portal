<%@page import="com.dao.SystemUser"%>
<%@page import="com.facade.impl.LoginFacade"%>
<%@page import="com.facade.ILoginFacade"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ include file="../template/header.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {

		jQuery("#changeSuccess").validationEngine();
	});
</script>

<%
	String requestParam = request.getParameter("request");
	boolean hasChanged = false;
	
	if ((requestParam != null) && (requestParam.equals("changePW"))) {

		String oldPassword = request.getParameter("oldPW");
		String newPassword = request.getParameter("newPW");
		String loginId = ((SystemUser) session
				.getAttribute("loggedInUser")).getUserName();

		ILoginFacade loginFacade = new LoginFacade();
		hasChanged = loginFacade.changePassword(loginId,
				oldPassword, newPassword);

		if (hasChanged == true) {
			response.sendRedirect("student_home.jsp");
		}
	}
%>

<div id="content">
	<img id="top" src="../img/top.png" alt="" />
	<div id="form_container">

		<form id="changeSuccess" class="appnitro" method="post"
			action="?request=changePW">
			<div class="form_description">
				<h2>Change Password :</h2>
				<p>Provide password information.</p>
			</div>
			<ul>

				<li id="li_1"><label class="description" for="oldPW">Old
						Password </label>
					<div>
						<input id="element_1" name="oldPW"
							class="validate[required,minSize[6],maxSize[12]] element text small"
							type="password" maxlength="20" value="" />
					</div></li>
				<li id="li_3"><label class="description" for="newPW">New
						Password </label>
					<div>
						<input id="element_3" name="newPW"
							class="validate[required,minSize[6],maxSize[12]] element text small"
							type="password" maxlength="20" value="" />
					</div></li>

				<li id="li_4"><label class="description" for="confirmNewPW">Confirm
						New Password </label>
					<div>
						<input id="element_4" name="confirmNewPW"
							class="validate[required,minSize[6],maxSize[12],equals[element_3]] element text small"
							type="password" maxlength="20" value="" />
					</div></li>

				<li class="buttons"><input id="saveForm" class="button_text"
					type="reset" name="reset" value="Reset" /> <input id="saveForm"
					class="button_text" type="submit" name="submit"
					value="Change Password" /></li>

			</ul>
		</form>

	</div>
	<img id="bottom" src="../img/bottom.png" alt="" />
</div>

<%@ include file="../template/footer.html"%>
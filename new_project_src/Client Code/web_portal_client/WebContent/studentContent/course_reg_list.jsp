<%@page import="com.dao.EnrollmentClientDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%
	String searchTxt = (String) request.getParameter("searchTxt");
	String searchCriteria = (String) request
			.getParameter("searchCriteria");

	String start = "";
	String end = "";

	if (searchCriteria.equals("idRange")) {
		String[] range = searchTxt.split("-");

		if (range.length == 2) {
			start = range[0].trim();
			end = range[1].trim();
		}
	}
%>
<c:set var="searchText" value="<%=searchTxt%>" />
<c:set var="criteria" value="<%=searchCriteria%>" />
<c:set var="rangeStart" value="<%=start%>" />
<c:set var="rangeEnd" value="<%=end%>" />

<sql:query var="courses" dataSource="jdbc/student_portal">
	<c:choose>
		<c:when test="${criteria == 'name'}">
			SELECT * FROM student_portal.course WHERE name like ?
			ORDER BY created_on DESC 
			LIMIT 0, 30
			<sql:param value="%${searchText}%" />
		</c:when>
		<c:when test="${criteria == 'term'}">
			SELECT * FROM student_portal.course WHERE term like ?
			ORDER BY created_on DESC 
			LIMIT 0, 30
			<sql:param value="%${searchText}%" />
		</c:when>
		<c:when test="${criteria == 'courseId'}">
			SELECT * FROM student_portal.course WHERE course_id=?
			ORDER BY created_on DESC 
			LIMIT 0, 30
			<sql:param value="${searchText}" />
		</c:when>
		<c:when test="${criteria == 'idRange'}">
			SELECT * FROM student_portal.course WHERE course_id BETWEEN ? AND ?
			ORDER BY created_on DESC 
			LIMIT 0, 30
			<sql:param value="${rangeStart}" />
			<sql:param value="${rangeEnd}" />
		</c:when>
	</c:choose>
</sql:query>


<%@ include file="../template/header.jsp"%>

<%
	EnrollmentClientDAO[] enrolledCourses = (EnrollmentClientDAO[]) session
			.getAttribute("enrolledCourses");

	List<Integer> failedCourses = new ArrayList<Integer>();

	if (enrolledCourses != null) {
		for (int i = 0; i < enrolledCourses.length; i++) {
			if (enrolledCourses[i].getFinalGrade().equals("Failed")) {
				failedCourses.add(enrolledCourses[i].getCourseId());
			}
		}
	}
%>
<div id="content">
	<form action="../MainServlet?request=enroll" method="post">
		<table class="gridtable">
			<tr>
				<th>Actions</th>
				<th>Course ID</th>
				<th>Course Name</th>
				<th>Term</th>
				<th>Campus Location</th>
				<th>Credits</th>
				<th>Enroll</th>
			</tr>

			<c:forEach var="course" items="${courses.rows}">
				<tr>
					<td style="text-align: center;"><a
						href="course_detail.jsp?id=${course.course_id}&cname=${course.name}"
						class="tooltip"> <img src="../img/view.gif" /> <span>
								<img class="callout" src="../img/callout.gif" /> <strong>View
									course content..</strong> <br /> <img src="../img/tooltip.JPG"
								style="float: right;" /> View course details including
								syllabus, assignments, notes and etc.
						</span>
					</a></td>

					<td>${course.course_id}</td>
					<td>${course.name}</td>
					<td>${course.term}</td>
					<td>${course.location}</td>
					<td>${course.credit}</td>
					<td><c:forEach var="registeredCourses"
							items="${enrolledCourses}">
							<c:if test="${registeredCourses.courseId == course.course_id}">

								<c:set var="isEnrolled" value="${true}" />
							</c:if>
						</c:forEach> <c:forEach var="failedCourse" items="${failedCourses}">
							<c:if test="${failedCourse != course.course_id}">

								<c:set var="isPassed" value="${true}" />
							</c:if>
						</c:forEach> <c:choose>
							<c:when test="${isEnrolled || isPassed}">
								<input type="checkbox" name="selectedCourses"
									value="${course.course_id}" disabled="disabled" />
							</c:when>
							<c:otherwise>
								<input type="checkbox" name="selectedCourses"
									value="${course.course_id}" />
							</c:otherwise>
						</c:choose></td>
				</tr>
			</c:forEach>
			<tr>
				<td></td>
				<td></td>
				<td><input type="reset" name="reset" value="Reset" /></td>
				<td><input type="submit" name="enroll" value="Enroll" /></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</form>
</div>

<%@ include file="../template/footer.html"%>
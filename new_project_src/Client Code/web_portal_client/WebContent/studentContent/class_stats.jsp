<%@page import="com.dao.SystemUser"%>
<%@page import="com.facade.impl.EnrollmentFacade"%>
<%@page import="com.facade.IEnrollmentFacade"%>
<%@page import="com.dao.GradeStatDAO"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ include file="../template/header.jsp"%>

<c:set var="courseId" value="<%= request.getParameter(\"courseId\") %>" />
<%
	int courseId = Integer.parseInt(request.getParameter("courseId"));
	SystemUser user = (SystemUser) session.getAttribute("loggedInUser");
	IEnrollmentFacade enrollmentFacade = new EnrollmentFacade();
	
	GradeStatDAO[] classStatsList = enrollmentFacade.getClassStats(courseId, user.getUserName());	
%>
<c:set var="statList" value="<%= classStatsList %>" />

<div id="content">
	<table class="gridtable">
		<tr>
			<th>Item Name</th>
			<th>Class Minimum Score</th>
			<th>Class Maximum Score</th>
			<th>Class Average Score</th>
			<th>Your Score</th>
		</tr>

		<c:forEach var="item" items="${statList}">
			<tr>
				<td>${item.practiceName}</td>
				<td>${item.minScore}</td>
				<td>${item.maxScore}</td>
				<td>${item.avgScore}</td>
				<td>${item.studentScore}</td>
			</tr>
		</c:forEach>
	</table>
</div>

<%@ include file="../template/footer.html"%>
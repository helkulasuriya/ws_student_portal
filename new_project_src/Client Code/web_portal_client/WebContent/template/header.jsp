<!DOCTYPE html>
<head>

<script type="text/javascript" src="../js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript" src="../js/jquery.popupWindow.js"></script>

<link rel="stylesheet" media="all" type="text/css"
	href="../css/jquery-ui.css" />

<link rel="stylesheet" media="all" type="text/css"
	href="../css/jquery-ui-timepicker-addon.css" />
<link media="all" type="text/css" href="../css/core.css"
	rel="stylesheet"></link>
<link media="all" type="text/css" href="../css/view.css"
	rel="stylesheet"></link>

<link media="all" type="text/css"
	href="../css/validationEngine.jquery.css" rel="stylesheet" />
<script src="../js/jquery.validationEngine.js" type="text/javascript"
	charset="utf-8"></script>
<script src="../js/jquery.validationEngine-en.js" type="text/javascript"
	charset="utf-8"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {

						$(document).tooltip();

						$('#img').mouseover(function() {

						});

						$(function() {
							$('.menu_link').click(function(e) {
								var requestMapping = $(this).attr('href');
							});
						});

						$('#course_form')
								.submit(
										function() {

											if ($('#course_title').val() == "") {

												$('#error')
														.html(
																"Course title cannot be empty!");
												return false;
											} else if ($('#course_location')
													.val() == "") {

												$('#error')
														.html(
																"Course location cannot be empty!");
												return false;
											} else if ($('#course_credits')
													.val() == "") {

												$('#error')
														.html(
																"Course credits cannot be empty!");
												return false;
											} else if (isNaN($(
													'#course_credits').val())) {

												$('#error')
														.html(
																"Course credits should be numaric!");
												return false;
											} else if ($('#course_time').val() != "") {

												if ($('#course_time').val()
														.split(':').length != 2
														|| $('#course_time')
																.val().split(
																		':')[1]
																.split(' ').length != 2) {
													$('#error')
															.html(
																	"Course schedule should be in correct format! HH:MM DAY");
													return false;
												}
											} else {
												$('#error').html("");
												alert('c');
											}

										});

					});

	function validadatePercentage() {

		var percentage = parseInt($('#final_exam_per').val())
				+ parseInt($('#assignment_per').val())
				+ parseInt($('#quezz_per').val())
				+ parseInt($('#midterm_per').val());

		if (isNaN(percentage) || percentage > 100) {
			alert('Invalid total !! sum of percentage should be 100% !');
			return false;
		} else {
			return true;
		}
	}

	function updateDate(type) {

		var id = '';
		var date = '';
		var date_time = '';
		var percentage = '';
		if (type == 0) {
			id = $('#final_exam_dt_id').val();

			percentage = $('#final_exam_per').val();

			if (isNaN(percentage) || percentage > 100 || percentage < 0) {
				alert('Invalid percentage in Final exam % !');
				$('#final_exam_per').focus();
				$('#final_exam_per').val('');
				return;
			}

			date = $('#final_exam_dt').val();
		} else if (type == 1) {
			id = $('#assignment_dt_id').val();

			percentage = $('#assignment_per').val();

			if (isNaN(percentage) || percentage > 100 || percentage < 0) {
				alert('Invalid percentage in Assignment exam % !');
				$('#assignment_per').focus();
				$('#assignment_per').val('');
				return;
			}
			date = $('#assignment_dt').val();
		} else if (type == 2) {
			id = $('#quezz_dt_id').val();

			percentage = $('#quezz_per').val();

			if (isNaN(percentage) || percentage > 100 || percentage < 0) {
				alert('Invalid percentage in Quezz exam % !');
				$('#quezz_per').focus();
				$('#quezz_per').val('');
				return;
			}

			date = $('#quezz_dt').val();
		} else if (type == 3) {
			id = $('#midterm_dt_id').val();

			percentage = $('#midterm_per').val();

			if (isNaN(percentage) || percentage > 100 || percentage < 0) {
				alert('Invalid percentage in Midtearm exam % !');
				$('#midterm_per').focus();
				$('#midterm_per').val('');
				return;
			}

			date = $('#midterm_dt').val();
		} else {

			return;
		}

		if (!validadatePercentage()) {
			return false;
		}

		$("body").addClass("loading");

		$.ajax({
			url : "../CourseContentManager",
			type : "GET",
			data : {
				course_id : getParameterByName('course_id'),
				course_date : date,
				practice_id : id,
				percentage_val : percentage
			},
			dataType : 'html'
		}).done(function(data) {
			alert(data);
		});

		$("body").removeClass("loading");

	}

	//  http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex
				.exec(location.search);
		return results == null ? "" : decodeURIComponent(results[1].replace(
				/\+/g, " "));
	}
</script>
</head>
<body>
	<div class="menue_div_top">
		<table>
			<tbody>
				<tr>
					<td><img id="avc_logo_image" src="../img/title_main.png"
						style="padding-bottom: 10px; border: 0px"></td>

					<td>

						<div class="menue_div">
							<ul class="Superfish menu">
								<c:if test="${loggedInUser != null}">
									<c:choose>
										<c:when
											test="${sessionScope.loggedInUser.userType} eq 'STUDENT'">
											<li class="selected current tab"><a class="menu_link"
												href="student_home.jsp">Home</a></li>
											<li class="selected current tab"><a class="menu_link"
												href="../MainServlet?request=register">Register for a
													courses</a></li>
										</c:when>
										<c:otherwise>
											<li class="selected current tab"><a class="menu_link"
												href="/teacher_home.jsp">Home</a></li>
										</c:otherwise>
									</c:choose>
								</c:if>

								<c:choose>
									<c:when
										test="${sessionScope.loggedInUser.userType} eq 'TEACHER'">
										<li class="selected current tab"><a class="menu_link"
											href="/register">Manage marks</a>

											<ul class="children">
												<li><a class="menu_link" href="/searchAnnouncements">Update
														course marks</a></li>
												<li><a class="menu_link" href="/searchAssignments">Update
														student marks</a></li>
											</ul></li>

									</c:when>
								</c:choose>

								<li class="tab"><a class="menu_link" href="">Search
										course</a>
									<ul class="children">
										<li><a class="menu_link"
											href="../MainServlet?request=register">By name</a></li>
										<li><a class="menu_link"
											href="../MainServlet?request=register">By code</a></li>
										<li><a class="menu_link"
											href="../MainServlet?request=register">By tearm</a></li>
									</ul></li>
								<c:if
									test="${(loggedInUser != null) && sessionScope.loggedInUser.userType eq 'STUDENT'}">
									<li class="tab"><a class="menu_link" href="/createMain">Create
											content</a>
										<ul class="children">
											<li><a class="menu_link" href="teacher_home.jsp">Create
													Announcement</a></li>
											<li><a class="menu_link" href="teacher_home.jsp">Create
													Assignment</a></li>
											<li><a class="menu_link" href="teacher_home.jsp">Create
													Subject</a></li>
										</ul></li>
								</c:if>
								<li class="tab"><a class="menu_link"
									href="/AdministrationMain">Administration</a>
									<ul class="children">
										<li><a class="menu_link" href="change_password.jsp">Change
												password</a></li>
									</ul></li>
								<c:if test="${sessionScope.loggedInUser.userType} eq 'STUDENT'">
									<li class="tab"><a class="menu_link"
										href="../studentContent/calculate_gpa.jsp">Calculate GPA</a></li>
								</c:if>
								<li class="selected current tab"><a class="menu_link"
									href="../MainServlet?request=logout">Logout</a></li>

							</ul>
						</div>
				</tr>
			</tbody>
		</table>
	</div>
	<hr />
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ page import="java.sql.Connection"%>
<%@ page import="javax.naming.Context"%>
<%@ page import="javax.naming.InitialContext"%>
<%@ page import="javax.naming.NamingException"%>
<%@ page import="javax.sql.DataSource"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="com.dao.SystemUser"%>

<%@ page session="true"%>

<%@ include file="../template/header.jsp"%>

<div id="welcomeMsg">${sessionScope.loggedInUser}</div>
<br />
<div class="menue_div_bottom">


	<div id="content">

		<table class="gridtable">

			<tr>
				<td><c:choose>
						<c:when
							test="${fn:toLowerCase(sessionScope.messageType) == fn:toLowerCase('ERROR')}">
							<div id='msg' style='text-align: center'>
								<img src="../img/error.jpg" /> <span>
									${sessionScope.message} </span>
							</div>
						</c:when>
						<c:when
							test="${fn:toLowerCase(sessionScope.messageType) == fn:toLowerCase('SUCCESS')}">
							<div id='msg' style='text-align: center'>
								<img src="../img/success.jpg" /> <span>
									${sessionScope.message} </span>
							</div>
						</c:when>
						<c:otherwise>
							<div id='msg' style='text-align: center'>
								<img src="../img/unknown.jpg" /> <span> Hmm.. something
									went wrong.. Please try again. </span>
							</div>
						</c:otherwise>
					</c:choose></td>
			</tr>

		</table>
		<%
			session.setAttribute("messageType", null);
			session.setAttribute("message", null);
		%>
	</div>

	<%@ include file="../template/footer.html"%>
Introduction
============
This application was developed to provide means for managing course
information of an institution. This web application has adopted the
techniques of MVC architecture in order to provide better performance,
maintainability and security to the content.

For further details please refer to the README.pdf file.

Contributors:
-------------
Isuru Chamarasinghe,
Helini Kulasuriya